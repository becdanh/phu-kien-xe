﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmChiTietNhomHang : Form
    {
        NhomHangHoaDTO nhomhang;
        public FrmChiTietNhomHang(NhomHangHoaDTO nhomhang = null)
        {
            InitializeComponent();
            this.nhomhang = nhomhang;
            if (nhomhang != null)
            {
                //Cap nhat
                lblTieuDe.Text = "CẬP NHẬT NHÓM HÀNG";
                txtMaNhomHang.Enabled = false;
                txtMaNhomHang.Text = nhomhang.MaNHH;
                txtTenNhomHang.Text = nhomhang.TenNHH;
                txtGhiChu.Text = nhomhang.GhiChu;
            }
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            using (var db = new PhuKienDB())
            {
                if (nhomhang == null)
                {
                    if (string.IsNullOrWhiteSpace(txtMaNhomHang.Text)
                        || string.IsNullOrWhiteSpace(txtTenNhomHang.Text))
                    {
                        MessageBox.Show("Vui lòng nhập đầy đủ thông tin.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (db.nhomHangHoas.Any(t => t.MaNHH == txtMaNhomHang.Text))
                    {
                        MessageBox.Show("Mã nhóm hàng hóa đã tồn tại. Vui lòng chọn mã khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //Thêm mới sinh viên
                    var sv = new NhomHangHoa
                    {
                        MaNHH = txtGhiChu.Text,
                        TenNHH = txtTenNhomHang.Text,
                        GhiChu = txtGhiChu.Text,
                    };
                    db.nhomHangHoas.Add(sv);
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;

                }
                else
                {
                    var sv = db.nhomHangHoas.Where(t => t.MaNHH == nhomhang.MaNHH).FirstOrDefault();
                    sv.TenNHH = txtTenNhomHang.Text;
                    sv.GhiChu = txtGhiChu.Text;
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
