﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmSoLuong : Form
    {
        public int SelectedQuantity { get; private set; }
        public int OriginalQuantity { get; private set; }


        public int MaxQuantity { get; set; }

        public FrmSoLuong(int initialQuantity, int maxQuantity)
        {
            InitializeComponent();

            OriginalQuantity = initialQuantity;


            MaxQuantity = maxQuantity;

            NuUpDownSoLuong.Value = initialQuantity;
            NuUpDownSoLuong.Maximum = maxQuantity;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            SelectedQuantity = OriginalQuantity;

            this.Close();
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            SelectedQuantity = (int)NuUpDownSoLuong.Value;

            if (SelectedQuantity > MaxQuantity)
            {
                MessageBox.Show($"Số lượng vượt quá giới hạn ({MaxQuantity}).", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return; // Do not close the form if the quantity is invalid
            }

            this.Close();
        }
    }
}
