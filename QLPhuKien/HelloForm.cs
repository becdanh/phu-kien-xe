﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Reporting.WinForms;
using QLPhuKien.DTO;
using QLPhuKien.Migrations;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QLPhuKien.FrmBanHang;

namespace QLPhuKien
{
    public partial class HelloForm : Form
    {
        private int HoaDonId;

        public HelloForm(int hoaDonId)
        {
            InitializeComponent();
            HoaDonId = hoaDonId;
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
                using (var context = new PhuKienDB())
                {
                    var query = from h in context.hoaDons
                                join c in context.khachHangs on h.CustomerId equals c.CustomerId
                                join ct in context.chiTietHoaDons on h.HoaDonId equals ct.HoaDonId
                                join hh in context.hangHoas on ct.Barcode equals hh.Barcode
                                join u in context.users on h.UserId equals u.UserId
                                where h.HoaDonId == HoaDonId
                                select new
                                {
                                    HoaDonId = h.HoaDonId,
                                    NgayTao = h.NgayTao,
                                    TongTien = h.TongTien,
                                    TenKhachHang = c.FullName,
                                    DiaChi = c.DiaChi,
                                    ChiTietHoaDonId = ct.ChiTietHoaDonId,
                                    Barcode = hh.Barcode,
                                    TenHangHoa = hh.TenHangHoa,
                                    SoLuongMua = ct.SoLuongMua,
                                    GiaBan = ct.GiaBan,
                                    ThanhTien = ct.ThanhTien,
                                    TenNhanVien = u.FullName
                                };

                    var reportData = query.ToList();


                    // Set the data source for the report
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", reportData));

                    // Set the report path for the embedded resource
                    string reportPath = "D:\\dotNet framework\\QLPhuKien\\QLPhuKien\\ReportHoaDon.rdlc";
                    reportViewer1.LocalReport.ReportPath = reportPath;

                    // Refresh the report
                    reportViewer1.RefreshReport();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
