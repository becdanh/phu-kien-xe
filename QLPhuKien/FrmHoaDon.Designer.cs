﻿namespace QLPhuKien
{
    partial class FrmHoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHoaDon));
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges6 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            dtgHoaDon = new Guna.UI2.WinForms.Guna2DataGridView();
            tenNhanVienDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            tenKhachHangDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            ngayTaoDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            tongTienDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            hoaDonDTOBindingSource = new BindingSource(components);
            btnXoa = new Guna.UI2.WinForms.Guna2Button();
            btnXemChiTiet = new Guna.UI2.WinForms.Guna2Button();
            txtTimKiem = new Guna.UI2.WinForms.Guna2TextBox();
            ((System.ComponentModel.ISupportInitialize)dtgHoaDon).BeginInit();
            ((System.ComponentModel.ISupportInitialize)hoaDonDTOBindingSource).BeginInit();
            SuspendLayout();
            // 
            // dtgHoaDon
            // 
            dtgHoaDon.AllowUserToAddRows = false;
            dtgHoaDon.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = Color.White;
            dtgHoaDon.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dtgHoaDon.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            dtgHoaDon.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = Color.FromArgb(232, 234, 237);
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = Color.FromArgb(232, 234, 237);
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            dtgHoaDon.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dtgHoaDon.ColumnHeadersHeight = 40;
            dtgHoaDon.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dtgHoaDon.Columns.AddRange(new DataGridViewColumn[] { tenNhanVienDataGridViewTextBoxColumn, tenKhachHangDataGridViewTextBoxColumn, ngayTaoDataGridViewTextBoxColumn, tongTienDataGridViewTextBoxColumn });
            dtgHoaDon.DataSource = hoaDonDTOBindingSource;
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = Color.White;
            dataGridViewCellStyle3.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = Color.FromArgb(71, 69, 94);
            dataGridViewCellStyle3.SelectionBackColor = Color.FromArgb(239, 241, 243);
            dataGridViewCellStyle3.SelectionForeColor = Color.FromArgb(71, 69, 94);
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
            dtgHoaDon.DefaultCellStyle = dataGridViewCellStyle3;
            dtgHoaDon.GridColor = Color.FromArgb(231, 229, 255);
            dtgHoaDon.Location = new Point(30, 105);
            dtgHoaDon.Name = "dtgHoaDon";
            dtgHoaDon.ReadOnly = true;
            dtgHoaDon.RowHeadersVisible = false;
            dtgHoaDon.RowHeadersWidth = 51;
            dtgHoaDon.RowTemplate.Height = 29;
            dtgHoaDon.Size = new Size(885, 530);
            dtgHoaDon.TabIndex = 26;
            dtgHoaDon.ThemeStyle.AlternatingRowsStyle.BackColor = Color.White;
            dtgHoaDon.ThemeStyle.AlternatingRowsStyle.Font = null;
            dtgHoaDon.ThemeStyle.AlternatingRowsStyle.ForeColor = Color.Empty;
            dtgHoaDon.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = Color.Empty;
            dtgHoaDon.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = Color.Empty;
            dtgHoaDon.ThemeStyle.BackColor = Color.White;
            dtgHoaDon.ThemeStyle.GridColor = Color.FromArgb(231, 229, 255);
            dtgHoaDon.ThemeStyle.HeaderStyle.BackColor = Color.FromArgb(100, 88, 255);
            dtgHoaDon.ThemeStyle.HeaderStyle.BorderStyle = DataGridViewHeaderBorderStyle.None;
            dtgHoaDon.ThemeStyle.HeaderStyle.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dtgHoaDon.ThemeStyle.HeaderStyle.ForeColor = Color.White;
            dtgHoaDon.ThemeStyle.HeaderStyle.HeaightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dtgHoaDon.ThemeStyle.HeaderStyle.Height = 40;
            dtgHoaDon.ThemeStyle.ReadOnly = true;
            dtgHoaDon.ThemeStyle.RowsStyle.BackColor = Color.White;
            dtgHoaDon.ThemeStyle.RowsStyle.BorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dtgHoaDon.ThemeStyle.RowsStyle.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dtgHoaDon.ThemeStyle.RowsStyle.ForeColor = Color.FromArgb(71, 69, 94);
            dtgHoaDon.ThemeStyle.RowsStyle.Height = 29;
            dtgHoaDon.ThemeStyle.RowsStyle.SelectionBackColor = Color.FromArgb(231, 229, 255);
            dtgHoaDon.ThemeStyle.RowsStyle.SelectionForeColor = Color.FromArgb(71, 69, 94);
            // 
            // tenNhanVienDataGridViewTextBoxColumn
            // 
            tenNhanVienDataGridViewTextBoxColumn.DataPropertyName = "TenNhanVien";
            tenNhanVienDataGridViewTextBoxColumn.HeaderText = "Nhân viên";
            tenNhanVienDataGridViewTextBoxColumn.MinimumWidth = 6;
            tenNhanVienDataGridViewTextBoxColumn.Name = "tenNhanVienDataGridViewTextBoxColumn";
            tenNhanVienDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tenKhachHangDataGridViewTextBoxColumn
            // 
            tenKhachHangDataGridViewTextBoxColumn.DataPropertyName = "TenKhachHang";
            tenKhachHangDataGridViewTextBoxColumn.HeaderText = "Khách hàng";
            tenKhachHangDataGridViewTextBoxColumn.MinimumWidth = 6;
            tenKhachHangDataGridViewTextBoxColumn.Name = "tenKhachHangDataGridViewTextBoxColumn";
            tenKhachHangDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ngayTaoDataGridViewTextBoxColumn
            // 
            ngayTaoDataGridViewTextBoxColumn.DataPropertyName = "NgayTao";
            ngayTaoDataGridViewTextBoxColumn.HeaderText = "Ngày tạo";
            ngayTaoDataGridViewTextBoxColumn.MinimumWidth = 6;
            ngayTaoDataGridViewTextBoxColumn.Name = "ngayTaoDataGridViewTextBoxColumn";
            ngayTaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tongTienDataGridViewTextBoxColumn
            // 
            tongTienDataGridViewTextBoxColumn.DataPropertyName = "TongTien";
            tongTienDataGridViewTextBoxColumn.HeaderText = "Tổng tiền";
            tongTienDataGridViewTextBoxColumn.MinimumWidth = 6;
            tongTienDataGridViewTextBoxColumn.Name = "tongTienDataGridViewTextBoxColumn";
            tongTienDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hoaDonDTOBindingSource
            // 
            hoaDonDTOBindingSource.DataSource = typeof(DTO.HoaDonDTO);
            // 
            // btnXoa
            // 
            btnXoa.AutoRoundedCorners = true;
            btnXoa.BorderRadius = 21;
            customizableEdges1.TopRight = false;
            btnXoa.CustomizableEdges = customizableEdges1;
            btnXoa.DisabledState.BorderColor = Color.DarkGray;
            btnXoa.DisabledState.CustomBorderColor = Color.DarkGray;
            btnXoa.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnXoa.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnXoa.FillColor = Color.FromArgb(50, 55, 89);
            btnXoa.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnXoa.ForeColor = Color.White;
            btnXoa.Location = new Point(142, 28);
            btnXoa.Name = "btnXoa";
            btnXoa.ShadowDecoration.CustomizableEdges = customizableEdges2;
            btnXoa.Size = new Size(94, 45);
            btnXoa.TabIndex = 25;
            btnXoa.Text = "Xóa";
            btnXoa.Click += btnXoa_Click;
            // 
            // btnXemChiTiet
            // 
            btnXemChiTiet.AutoRoundedCorners = true;
            btnXemChiTiet.BorderRadius = 21;
            customizableEdges3.TopRight = false;
            btnXemChiTiet.CustomizableEdges = customizableEdges3;
            btnXemChiTiet.DisabledState.BorderColor = Color.DarkGray;
            btnXemChiTiet.DisabledState.CustomBorderColor = Color.DarkGray;
            btnXemChiTiet.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnXemChiTiet.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnXemChiTiet.FillColor = Color.FromArgb(254, 205, 220);
            btnXemChiTiet.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnXemChiTiet.ForeColor = Color.Black;
            btnXemChiTiet.Location = new Point(30, 28);
            btnXemChiTiet.Name = "btnXemChiTiet";
            btnXemChiTiet.ShadowDecoration.CustomizableEdges = customizableEdges4;
            btnXemChiTiet.Size = new Size(90, 45);
            btnXemChiTiet.TabIndex = 24;
            btnXemChiTiet.Text = "Chi tiết";
            btnXemChiTiet.Click += btnXemChiTiet_Click;
            // 
            // txtTimKiem
            // 
            txtTimKiem.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            txtTimKiem.AutoRoundedCorners = true;
            txtTimKiem.BorderRadius = 21;
            txtTimKiem.CustomizableEdges = customizableEdges5;
            txtTimKiem.DefaultText = "";
            txtTimKiem.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtTimKiem.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtTimKiem.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtTimKiem.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtTimKiem.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTimKiem.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtTimKiem.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTimKiem.IconLeft = (Image)resources.GetObject("txtTimKiem.IconLeft");
            txtTimKiem.IconLeftOffset = new Point(5, 0);
            txtTimKiem.Location = new Point(601, 28);
            txtTimKiem.Name = "txtTimKiem";
            txtTimKiem.PasswordChar = '\0';
            txtTimKiem.PlaceholderText = "Search in the bóc";
            txtTimKiem.SelectedText = "";
            txtTimKiem.ShadowDecoration.CustomizableEdges = customizableEdges6;
            txtTimKiem.Size = new Size(319, 45);
            txtTimKiem.TabIndex = 23;
            txtTimKiem.KeyPress += txtTimKiem_KeyPress;
            // 
            // FrmHoaDon
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(940, 662);
            Controls.Add(dtgHoaDon);
            Controls.Add(btnXoa);
            Controls.Add(btnXemChiTiet);
            Controls.Add(txtTimKiem);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmHoaDon";
            Text = "FrmHoaDon";
            Load += FrmHoaDon_Load;
            ((System.ComponentModel.ISupportInitialize)dtgHoaDon).EndInit();
            ((System.ComponentModel.ISupportInitialize)hoaDonDTOBindingSource).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn;
        private Guna.UI2.WinForms.Guna2DataGridView dtgHoaDon;
        private Guna.UI2.WinForms.Guna2Button btnXoa;
        private Guna.UI2.WinForms.Guna2Button btnXemChiTiet;
        private Guna.UI2.WinForms.Guna2TextBox txtTimKiem;
        private BindingSource hoaDonDTOBindingSource;
        private DataGridViewTextBoxColumn tenNhanVienDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn tenKhachHangDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn ngayTaoDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn tongTienDataGridViewTextBoxColumn;
    }
}