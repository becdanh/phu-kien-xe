﻿namespace QLPhuKien
{
    partial class FrmChiTietNhomHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges15 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges16 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges21 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges22 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges17 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges18 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges19 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges20 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges23 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges24 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges25 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges26 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges27 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges28 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            lblTieuDe = new Label();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            btnBoQua = new Guna.UI2.WinForms.Guna2Button();
            btnDongY = new Guna.UI2.WinForms.Guna2Button();
            label2 = new Label();
            txtMaNhomHang = new Guna.UI2.WinForms.Guna2TextBox();
            txtTenNhomHang = new Guna.UI2.WinForms.Guna2TextBox();
            label3 = new Label();
            txtGhiChu = new Guna.UI2.WinForms.Guna2TextBox();
            label4 = new Label();
            guna2Panel1.SuspendLayout();
            guna2Panel2.SuspendLayout();
            SuspendLayout();
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges15;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges16;
            guna2Panel1.Size = new Size(509, 117);
            guna2Panel1.TabIndex = 1;
            // 
            // lblTieuDe
            // 
            lblTieuDe.AutoSize = true;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(107, 37);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(289, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "THÊM NHÓM HÀNG";
            // 
            // guna2Panel2
            // 
            guna2Panel2.BackColor = Color.Gainsboro;
            guna2Panel2.Controls.Add(btnBoQua);
            guna2Panel2.Controls.Add(btnDongY);
            guna2Panel2.CustomizableEdges = customizableEdges21;
            guna2Panel2.Dock = DockStyle.Bottom;
            guna2Panel2.Location = new Point(0, 351);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges22;
            guna2Panel2.Size = new Size(509, 76);
            guna2Panel2.TabIndex = 1;
            // 
            // btnBoQua
            // 
            btnBoQua.AutoRoundedCorners = true;
            btnBoQua.BorderRadius = 21;
            customizableEdges17.TopRight = false;
            btnBoQua.CustomizableEdges = customizableEdges17;
            btnBoQua.DisabledState.BorderColor = Color.DarkGray;
            btnBoQua.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBoQua.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBoQua.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBoQua.FillColor = Color.FromArgb(50, 55, 89);
            btnBoQua.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnBoQua.ForeColor = Color.White;
            btnBoQua.Location = new Point(370, 19);
            btnBoQua.Name = "btnBoQua";
            btnBoQua.ShadowDecoration.CustomizableEdges = customizableEdges18;
            btnBoQua.Size = new Size(94, 45);
            btnBoQua.TabIndex = 1;
            btnBoQua.Text = "Bỏ qua";
            btnBoQua.Click += btnBoQua_Click;
            // 
            // btnDongY
            // 
            btnDongY.AutoRoundedCorners = true;
            btnDongY.BorderRadius = 21;
            customizableEdges19.TopRight = false;
            btnDongY.CustomizableEdges = customizableEdges19;
            btnDongY.DisabledState.BorderColor = Color.DarkGray;
            btnDongY.DisabledState.CustomBorderColor = Color.DarkGray;
            btnDongY.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnDongY.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnDongY.FillColor = Color.FromArgb(254, 205, 220);
            btnDongY.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnDongY.ForeColor = Color.Black;
            btnDongY.Location = new Point(258, 19);
            btnDongY.Name = "btnDongY";
            btnDongY.ShadowDecoration.CustomizableEdges = customizableEdges20;
            btnDongY.Size = new Size(90, 45);
            btnDongY.TabIndex = 0;
            btnDongY.Text = "Đồng ý";
            btnDongY.Click += btnDongY_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(11, 147);
            label2.Name = "label2";
            label2.Size = new Size(109, 20);
            label2.TabIndex = 7;
            label2.Text = "Mã nhóm hàng";
            // 
            // txtMaNhomHang
            // 
            txtMaNhomHang.CustomizableEdges = customizableEdges23;
            txtMaNhomHang.DefaultText = "";
            txtMaNhomHang.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtMaNhomHang.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtMaNhomHang.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtMaNhomHang.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtMaNhomHang.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtMaNhomHang.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtMaNhomHang.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtMaNhomHang.Location = new Point(138, 137);
            txtMaNhomHang.Name = "txtMaNhomHang";
            txtMaNhomHang.PasswordChar = '\0';
            txtMaNhomHang.PlaceholderText = "";
            txtMaNhomHang.SelectedText = "";
            txtMaNhomHang.ShadowDecoration.CustomizableEdges = customizableEdges24;
            txtMaNhomHang.Size = new Size(326, 43);
            txtMaNhomHang.TabIndex = 0;
            // 
            // txtTenNhomHang
            // 
            txtTenNhomHang.CustomizableEdges = customizableEdges25;
            txtTenNhomHang.DefaultText = "";
            txtTenNhomHang.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtTenNhomHang.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtTenNhomHang.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtTenNhomHang.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtTenNhomHang.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenNhomHang.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtTenNhomHang.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenNhomHang.Location = new Point(138, 212);
            txtTenNhomHang.Name = "txtTenNhomHang";
            txtTenNhomHang.PasswordChar = '\0';
            txtTenNhomHang.PlaceholderText = "";
            txtTenNhomHang.SelectedText = "";
            txtTenNhomHang.ShadowDecoration.CustomizableEdges = customizableEdges26;
            txtTenNhomHang.Size = new Size(326, 43);
            txtTenNhomHang.TabIndex = 1;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(11, 221);
            label3.Name = "label3";
            label3.Size = new Size(111, 20);
            label3.TabIndex = 7;
            label3.Text = "Tên nhóm hàng";
            // 
            // txtGhiChu
            // 
            txtGhiChu.CustomizableEdges = customizableEdges27;
            txtGhiChu.DefaultText = "";
            txtGhiChu.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtGhiChu.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtGhiChu.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtGhiChu.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtGhiChu.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtGhiChu.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtGhiChu.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtGhiChu.Location = new Point(138, 288);
            txtGhiChu.Name = "txtGhiChu";
            txtGhiChu.PasswordChar = '\0';
            txtGhiChu.PlaceholderText = "";
            txtGhiChu.SelectedText = "";
            txtGhiChu.ShadowDecoration.CustomizableEdges = customizableEdges28;
            txtGhiChu.Size = new Size(326, 43);
            txtGhiChu.TabIndex = 2;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(11, 299);
            label4.Name = "label4";
            label4.Size = new Size(58, 20);
            label4.TabIndex = 7;
            label4.Text = "Ghi chú";
            // 
            // FrmChiTietNhomHang
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.White;
            CancelButton = btnDongY;
            ClientSize = new Size(509, 427);
            Controls.Add(txtGhiChu);
            Controls.Add(label4);
            Controls.Add(txtTenNhomHang);
            Controls.Add(label3);
            Controls.Add(txtMaNhomHang);
            Controls.Add(label2);
            Controls.Add(guna2Panel2);
            Controls.Add(guna2Panel1);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmChiTietNhomHang";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FrmChiTietNhomHang";
            guna2Panel1.ResumeLayout(false);
            guna2Panel1.PerformLayout();
            guna2Panel2.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Label lblTieuDe;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Button btnBoQua;
        private Guna.UI2.WinForms.Guna2Button btnDongY;
        private Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txtMaNhomHang;
        private Guna.UI2.WinForms.Guna2TextBox txtTenNhomHang;
        private Label label3;
        private Guna.UI2.WinForms.Guna2TextBox txtGhiChu;
        private Label label4;
    }
}