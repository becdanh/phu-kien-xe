﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmHangHoa1 : Form
    {
        public FrmHangHoa1()
        {
            InitializeComponent();
        }
        void LoadNhomHangHoa()
        {
            PhuKienDB db = new PhuKienDB();
            var ls = db.nhomHangHoas.Select(e => new NhomHangHoaDTO
            {
                MaNHH = e.MaNHH,
                TenNHH = e.TenNHH,
                GhiChu = e.GhiChu,
            }).ToList();
            cbbNhomHangHoa.DataSource = ls;
            cbbNhomHangHoa.DisplayMember = "TenNHH";
        }

        void LoadHangHoa()
        {

            var selectedNhomHangHoa = cbbNhomHangHoa.SelectedItem as NhomHangHoaDTO;

            if (selectedNhomHangHoa != null)
            {
                PhuKienDB db = new PhuKienDB();
                var ls = db.hangHoas
                    .Where(t => t.MaNHH == selectedNhomHangHoa.MaNHH)
                    .Select(t => new HangHoaDTO
                    {
                        Barcode = t.Barcode,
                        TenHangHoa = t.TenHangHoa,
                        GiaBan = t.GiaBan,
                        DonVi = t.DonVi,
                        SoLuong = t.SoLuong,
                        MaNHH = t.MaNHH,
                        MaNCC = t.MaNCC,
                        Disabled = t.Disabled,
                        Anh = t.Anh,
                    })
                    .ToList();

                hangHoaDTOBindingSource.DataSource = ls;
            }
            else
            {
                hangHoaDTOBindingSource.DataSource = null;
            }

        }
        private void FrmHangHoa1_Load(object sender, EventArgs e)
        {
            LoadNhomHangHoa();
        }

        private void cbbNhomHangHoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadHangHoa();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var f = new FrmChiTietHangHoa1();
            if (f.ShowDialog() == DialogResult.OK)
            {
                LoadHangHoa();
                LoadNhomHangHoa();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            var sv = hangHoaDTOBindingSource.Current as HangHoaDTO;
            if (sv != null)
            {
                var f = new FrmChiTietHangHoa1(sv);
                if (f.ShowDialog() == DialogResult.OK)
                    LoadHangHoa();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var sv = hangHoaDTOBindingSource.Current as HangHoaDTO;
            if (sv != null)
            {
                // xac nhan xoa
                var rs = MessageBox.Show(
                    "Xác nhận xóa?",
                    "Thông báo",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question
                    );
                if (rs == DialogResult.OK)
                {
                    PhuKienDB db = new PhuKienDB();
                    var obj = db.hangHoas.Where(t => t.Barcode == sv.Barcode).FirstOrDefault();
                    if (obj != null)
                    {
                        db.hangHoas.Remove(obj);
                        db.SaveChanges();
                        LoadHangHoa();
                    }
                }
            }
        }

        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                var keyword = txtTimKiem.Text.ToLower();

                using (var db = new PhuKienDB())
                {
                    var query = db.hangHoas
                        .Where(t =>
                            t.Barcode.ToLower().Contains(keyword) ||
                            t.TenHangHoa.ToLower().Contains(keyword) ||
                            t.GiaBan.ToString().Contains(keyword) ||
                            t.DonVi.ToLower().Contains(keyword) ||
                            t.SoLuong.ToString().Contains(keyword) ||
                            t.MaNHH.ToLower().Contains(keyword) ||
                            t.MaNCC.ToLower().Contains(keyword)
                        )
                        .Select(t => new HangHoaDTO
                        {
                            Barcode = t.Barcode,
                            TenHangHoa = t.TenHangHoa,
                            GiaBan = t.GiaBan,
                            DonVi = t.DonVi,
                            SoLuong = t.SoLuong,
                            MaNHH = t.MaNHH,
                            MaNCC = t.MaNCC,
                            Disabled = t.Disabled,
                            Anh = t.Anh,
                        })
                        .ToList();

                    hangHoaDTOBindingSource.DataSource = query;
                }
            }
        }


    }
}
