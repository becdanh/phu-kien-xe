﻿namespace QLPhuKien
{
    partial class FrmSoLuong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges9 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges10 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges15 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges16 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges11 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges12 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges13 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges14 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            lblTieuDe = new Label();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            btnBoQua = new Guna.UI2.WinForms.Guna2Button();
            btnDongY = new Guna.UI2.WinForms.Guna2Button();
            label2 = new Label();
            NuUpDownSoLuong = new NumericUpDown();
            guna2Panel1.SuspendLayout();
            guna2Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)NuUpDownSoLuong).BeginInit();
            SuspendLayout();
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges9;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges10;
            guna2Panel1.Size = new Size(502, 118);
            guna2Panel1.TabIndex = 38;
            // 
            // lblTieuDe
            // 
            lblTieuDe.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(34, 35);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(436, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "ĐỒ GÌ ĐÓ";
            lblTieuDe.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // guna2Panel2
            // 
            guna2Panel2.BackColor = Color.Gainsboro;
            guna2Panel2.Controls.Add(btnBoQua);
            guna2Panel2.Controls.Add(btnDongY);
            guna2Panel2.CustomizableEdges = customizableEdges15;
            guna2Panel2.Dock = DockStyle.Bottom;
            guna2Panel2.Location = new Point(0, 248);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges16;
            guna2Panel2.Size = new Size(502, 76);
            guna2Panel2.TabIndex = 39;
            // 
            // btnBoQua
            // 
            btnBoQua.Anchor = AnchorStyles.None;
            btnBoQua.AutoRoundedCorners = true;
            btnBoQua.BorderRadius = 21;
            customizableEdges11.TopRight = false;
            btnBoQua.CustomizableEdges = customizableEdges11;
            btnBoQua.DisabledState.BorderColor = Color.DarkGray;
            btnBoQua.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBoQua.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBoQua.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBoQua.FillColor = Color.FromArgb(50, 55, 89);
            btnBoQua.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnBoQua.ForeColor = Color.White;
            btnBoQua.Location = new Point(382, 16);
            btnBoQua.Name = "btnBoQua";
            btnBoQua.ShadowDecoration.CustomizableEdges = customizableEdges12;
            btnBoQua.Size = new Size(94, 45);
            btnBoQua.TabIndex = 6;
            btnBoQua.Text = "Bỏ qua";
            btnBoQua.Click += btnBoQua_Click;
            // 
            // btnDongY
            // 
            btnDongY.Anchor = AnchorStyles.None;
            btnDongY.AutoRoundedCorners = true;
            btnDongY.BorderRadius = 21;
            customizableEdges13.TopRight = false;
            btnDongY.CustomizableEdges = customizableEdges13;
            btnDongY.DisabledState.BorderColor = Color.DarkGray;
            btnDongY.DisabledState.CustomBorderColor = Color.DarkGray;
            btnDongY.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnDongY.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnDongY.FillColor = Color.FromArgb(254, 205, 220);
            btnDongY.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnDongY.ForeColor = Color.Black;
            btnDongY.Location = new Point(271, 16);
            btnDongY.Name = "btnDongY";
            btnDongY.ShadowDecoration.CustomizableEdges = customizableEdges14;
            btnDongY.Size = new Size(90, 45);
            btnDongY.TabIndex = 5;
            btnDongY.Text = "Đồng ý";
            btnDongY.Click += btnDongY_Click;
            // 
            // label2
            // 
            label2.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(3, 163);
            label2.Name = "label2";
            label2.Size = new Size(146, 43);
            label2.TabIndex = 40;
            label2.Text = "Số lượng";
            label2.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // NuUpDownSoLuong
            // 
            NuUpDownSoLuong.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            NuUpDownSoLuong.Location = new Point(152, 165);
            NuUpDownSoLuong.Name = "NuUpDownSoLuong";
            NuUpDownSoLuong.Size = new Size(324, 43);
            NuUpDownSoLuong.TabIndex = 41;
            // 
            // FrmSoLuong
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(502, 324);
            Controls.Add(NuUpDownSoLuong);
            Controls.Add(guna2Panel1);
            Controls.Add(guna2Panel2);
            Controls.Add(label2);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmSoLuong";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FrmSoLuong";
            guna2Panel1.ResumeLayout(false);
            guna2Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)NuUpDownSoLuong).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Label lblTieuDe;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Button btnBoQua;
        private Guna.UI2.WinForms.Guna2Button btnDongY;
        private Label label2;
        private NumericUpDown NuUpDownSoLuong;
    }
}