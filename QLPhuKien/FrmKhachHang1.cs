﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmKhachHang1 : Form
    {
        public FrmKhachHang1()
        {
            InitializeComponent();
        }

        void LoadKhachHang()
        {

            PhuKienDB db = new PhuKienDB();
            var ls = db.khachHangs
                .Select(t => new KhachHangDTO
                {
                    CustomerId = t.CustomerId,
                    FullName = t.FullName,
                    SDT = t.SDT,
                    DiaChi = t.DiaChi,
                }).ToList();
            khachHangDTOBindingSource.DataSource = ls;

        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            var f = new FrmChiTietKhachKhang();
            if (f.ShowDialog() == DialogResult.OK)
            {
                LoadKhachHang();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            var sv = khachHangDTOBindingSource.Current as KhachHangDTO;
            if (sv != null)
            {
                var f = new FrmChiTietKhachKhang(sv);
                if (f.ShowDialog() == DialogResult.OK)
                    LoadKhachHang();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var sv = khachHangDTOBindingSource.Current as KhachHangDTO;
            if (sv != null)
            {
                // xac nhan xoa
                var rs = MessageBox.Show(
                    "Xác nhận xóa?",
                    "Thông báo",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question
                    );
                if (rs == DialogResult.OK)
                {
                    PhuKienDB db = new PhuKienDB();
                    var obj = db.khachHangs.Where(t => t.CustomerId == sv.CustomerId).FirstOrDefault();
                    if (obj != null)
                    {
                        db.khachHangs.Remove(obj);
                        db.SaveChanges();
                        LoadKhachHang();
                    }
                }
            }
        }

        private void FrmKhachHang1_Load(object sender, EventArgs e)
        {
            LoadKhachHang();
        }

        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            var tukhoa = txtTimKiem.Text.ToLower();
            using (var db = new PhuKienDB())
            {
                var ls = db.khachHangs
                    .Where(t =>
                    (t.FullName.ToLower().Contains(tukhoa.ToLower())
                    || t.SDT.ToLower() == tukhoa.ToLower()
                    || t.DiaChi.ToLower() == tukhoa.ToLower()
                ))
                .Select(t => new KhachHangDTO
                {
                    CustomerId = t.CustomerId,
                    FullName = t.FullName,
                    SDT = t.SDT,
                    DiaChi = t.DiaChi,
                }).ToList();
                khachHangDTOBindingSource.DataSource = null;
                khachHangDTOBindingSource.DataSource = ls;
            }
        }
    }
}
