﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmChiTietHoaDon : Form
    {
        private HoaDonDTO hoaDonDTO;
        public FrmChiTietHoaDon(HoaDonDTO hoaDonDTO)
        {
            InitializeComponent();
            this.hoaDonDTO = hoaDonDTO;
            LoadChiTietHoaDon();
            LoadHoaDonInfo();
        }

        private void LoadChiTietHoaDon()
        {
            using (PhuKienDB db = new PhuKienDB())
            {
                // Fetch details of the specified invoice from the database
                var chiTietHoaDonList = db.chiTietHoaDons
                    .Where(ct => ct.HoaDonId == hoaDonDTO.HoaDonId)
                    .Select(ct => new ChiTietHoaDonDTO
                    {
                        ChiTietHoaDonId = ct.ChiTietHoaDonId,
                        HoaDonId = ct.HoaDonId,
                        Barcode = ct.Barcode,
                        TenHH = ct.HangHoa.TenHangHoa,
                        SoLuongMua = ct.SoLuongMua,
                        GiaBan = ct.GiaBan,
                    })
                    .ToList();
                chiTietHoaDonDTOBindingSource.DataSource = chiTietHoaDonList;
            }
        }

        private void LoadHoaDonInfo()
        {
            // Display additional invoice information
            lblTenNhanVien.Text = hoaDonDTO.TenNhanVien;
            lblTenKhachHang.Text = hoaDonDTO.TenKhachHang;
            lblNgayLapHoaDon.Text = hoaDonDTO.NgayTao.ToString("dd/MM/yyyy");
            lblTongTien.Text = hoaDonDTO.TongTien.ToString("C");
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInHD_Click(object sender, EventArgs e)
        {
            HelloForm helloForm = new HelloForm(hoaDonDTO.HoaDonId);
            helloForm.ShowDialog();
        }
    }
}
