﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmMain : Form
    {
        private FrmDangNhap loginForm;
        public FrmMain()
        {
            InitializeComponent();
            loginForm = new FrmDangNhap();
            container(new FrmDashBoard());
            label_val.Text = "TRANG CHỦ";
        }

        private void container(object _form)
        {
            if (guna2Panel2.Controls.Count > 0) guna2Panel2.Controls.Clear();
            Form fm = _form as Form;
            fm.TopLevel = false;
            fm.FormBorderStyle = FormBorderStyle.None;
            fm.Dock = DockStyle.Fill;
            guna2Panel2.Controls.Add(fm);
            fm.Show();
        }
        private void btnNhaCungCap_Click(object sender, EventArgs e)
        {
            container(new FrmNhaCungCap1());
            label_val.Text = "NHÀ CUNG CẤP";
        }

        private void btnNhomHangHoa_Click(object sender, EventArgs e)
        {
            container(new FrmNhomHang());
            label_val.Text = "NHÓM HÀNG HÓA";
        }
        private void btnHangHoa_Click(object sender, EventArgs e)
        {
            container(new FrmHangHoa1());
            label_val.Text = "HÀNG HÓA";
        }

        private void btnKhachHang_Click(object sender, EventArgs e)
        {
            container(new FrmKhachHang1());
            label_val.Text = "KHÁCH HÀNG";
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            this.Hide();

            // Show the login form
            loginForm.Show();
        }
        private void guna2ControlBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnHoaDon_Click(object sender, EventArgs e)
        {
            container(new FrmHoaDon());
            label_val.Text = "QUẢN LÝ HÓA ĐƠN";
        }

        private void btnBanHang_Click(object sender, EventArgs e)
        {
            FrmBanHang frmbanhang = new FrmBanHang();
            frmbanhang.Show();
        }

        private void btnNhanVien_Click(object sender, EventArgs e)
        {
            if (FrmDangNhap.LoggedInUser != null && FrmDangNhap.LoggedInUser.IsAdmin)
            {
                container(new FrmNhanVien());
                label_val.Text = "QUẢN LÝ NHÂN VIÊN";
            }
            else
            {
                MessageBox.Show("Bạn không có đủ quyền để thực hiện thao tác này.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnOverview_Click(object sender, EventArgs e)
        {
            container(new FrmDashBoard());
            label_val.Text = "TRANG CHỦ";
        }
    }
}
