﻿namespace QLPhuKien
{
    partial class FrmChiTietNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges7 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges8 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges6 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges9 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges10 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges11 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges12 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges13 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges14 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            lblTieuDe = new Label();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            btnBoQua = new Guna.UI2.WinForms.Guna2Button();
            btnDongY = new Guna.UI2.WinForms.Guna2Button();
            txtPassword = new Guna.UI2.WinForms.Guna2TextBox();
            label4 = new Label();
            txtUsername = new Guna.UI2.WinForms.Guna2TextBox();
            label3 = new Label();
            txtTenNhanVien = new Guna.UI2.WinForms.Guna2TextBox();
            label2 = new Label();
            checkboxNV = new Guna.UI2.WinForms.Guna2CheckBox();
            guna2Panel1.SuspendLayout();
            guna2Panel2.SuspendLayout();
            SuspendLayout();
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges1;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges2;
            guna2Panel1.Size = new Size(508, 118);
            guna2Panel1.TabIndex = 29;
            // 
            // lblTieuDe
            // 
            lblTieuDe.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTieuDe.AutoSize = true;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(83, 35);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(327, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "THÊM MỚI NHÂN VIÊN";
            // 
            // guna2Panel2
            // 
            guna2Panel2.BackColor = Color.Gainsboro;
            guna2Panel2.Controls.Add(btnBoQua);
            guna2Panel2.Controls.Add(btnDongY);
            guna2Panel2.CustomizableEdges = customizableEdges7;
            guna2Panel2.Dock = DockStyle.Bottom;
            guna2Panel2.Location = new Point(0, 351);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges8;
            guna2Panel2.Size = new Size(508, 76);
            guna2Panel2.TabIndex = 30;
            // 
            // btnBoQua
            // 
            btnBoQua.Anchor = AnchorStyles.None;
            btnBoQua.AutoRoundedCorners = true;
            btnBoQua.BorderRadius = 21;
            customizableEdges3.TopRight = false;
            btnBoQua.CustomizableEdges = customizableEdges3;
            btnBoQua.DisabledState.BorderColor = Color.DarkGray;
            btnBoQua.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBoQua.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBoQua.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBoQua.FillColor = Color.FromArgb(50, 55, 89);
            btnBoQua.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnBoQua.ForeColor = Color.White;
            btnBoQua.Location = new Point(382, 19);
            btnBoQua.Name = "btnBoQua";
            btnBoQua.ShadowDecoration.CustomizableEdges = customizableEdges4;
            btnBoQua.Size = new Size(94, 45);
            btnBoQua.TabIndex = 6;
            btnBoQua.Text = "Bỏ qua";
            btnBoQua.Click += btnBoQua_Click;
            // 
            // btnDongY
            // 
            btnDongY.Anchor = AnchorStyles.None;
            btnDongY.AutoRoundedCorners = true;
            btnDongY.BorderRadius = 21;
            customizableEdges5.TopRight = false;
            btnDongY.CustomizableEdges = customizableEdges5;
            btnDongY.DisabledState.BorderColor = Color.DarkGray;
            btnDongY.DisabledState.CustomBorderColor = Color.DarkGray;
            btnDongY.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnDongY.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnDongY.FillColor = Color.FromArgb(254, 205, 220);
            btnDongY.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnDongY.ForeColor = Color.Black;
            btnDongY.Location = new Point(269, 16);
            btnDongY.Name = "btnDongY";
            btnDongY.ShadowDecoration.CustomizableEdges = customizableEdges6;
            btnDongY.Size = new Size(90, 45);
            btnDongY.TabIndex = 5;
            btnDongY.Text = "Đồng ý";
            btnDongY.Click += btnDongY_Click;
            // 
            // txtPassword
            // 
            txtPassword.CustomizableEdges = customizableEdges9;
            txtPassword.DefaultText = "";
            txtPassword.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtPassword.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtPassword.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtPassword.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtPassword.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtPassword.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtPassword.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtPassword.Location = new Point(131, 288);
            txtPassword.Name = "txtPassword";
            txtPassword.PasswordChar = '\0';
            txtPassword.PlaceholderText = "";
            txtPassword.SelectedText = "";
            txtPassword.ShadowDecoration.CustomizableEdges = customizableEdges10;
            txtPassword.Size = new Size(242, 43);
            txtPassword.TabIndex = 2;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(12, 298);
            label4.Name = "label4";
            label4.Size = new Size(70, 20);
            label4.TabIndex = 35;
            label4.Text = "Password";
            // 
            // txtUsername
            // 
            txtUsername.CustomizableEdges = customizableEdges11;
            txtUsername.DefaultText = "";
            txtUsername.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtUsername.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtUsername.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtUsername.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtUsername.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtUsername.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtUsername.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtUsername.Location = new Point(131, 212);
            txtUsername.Name = "txtUsername";
            txtUsername.PasswordChar = '\0';
            txtUsername.PlaceholderText = "";
            txtUsername.SelectedText = "";
            txtUsername.ShadowDecoration.CustomizableEdges = customizableEdges12;
            txtUsername.Size = new Size(345, 43);
            txtUsername.TabIndex = 1;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(12, 222);
            label3.Name = "label3";
            label3.Size = new Size(75, 20);
            label3.TabIndex = 33;
            label3.Text = "Username";
            // 
            // txtTenNhanVien
            // 
            txtTenNhanVien.CustomizableEdges = customizableEdges13;
            txtTenNhanVien.DefaultText = "";
            txtTenNhanVien.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtTenNhanVien.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtTenNhanVien.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtTenNhanVien.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtTenNhanVien.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenNhanVien.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtTenNhanVien.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenNhanVien.Location = new Point(131, 137);
            txtTenNhanVien.Name = "txtTenNhanVien";
            txtTenNhanVien.PasswordChar = '\0';
            txtTenNhanVien.PlaceholderText = "";
            txtTenNhanVien.SelectedText = "";
            txtTenNhanVien.ShadowDecoration.CustomizableEdges = customizableEdges14;
            txtTenNhanVien.Size = new Size(345, 43);
            txtTenNhanVien.TabIndex = 0;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 147);
            label2.Name = "label2";
            label2.Size = new Size(99, 20);
            label2.TabIndex = 31;
            label2.Text = "Tên nhân viên";
            // 
            // checkboxNV
            // 
            checkboxNV.AutoSize = true;
            checkboxNV.CheckedState.BorderColor = Color.FromArgb(94, 148, 255);
            checkboxNV.CheckedState.BorderRadius = 0;
            checkboxNV.CheckedState.BorderThickness = 0;
            checkboxNV.CheckedState.FillColor = Color.FromArgb(94, 148, 255);
            checkboxNV.Location = new Point(394, 298);
            checkboxNV.Name = "checkboxNV";
            checkboxNV.Size = new Size(85, 24);
            checkboxNV.TabIndex = 3;
            checkboxNV.Text = "IsAdmin";
            checkboxNV.UncheckedState.BorderColor = Color.FromArgb(125, 137, 149);
            checkboxNV.UncheckedState.BorderRadius = 0;
            checkboxNV.UncheckedState.BorderThickness = 0;
            checkboxNV.UncheckedState.FillColor = Color.White;
            // 
            // FrmChiTietNhanVien
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(508, 427);
            Controls.Add(checkboxNV);
            Controls.Add(guna2Panel1);
            Controls.Add(guna2Panel2);
            Controls.Add(txtPassword);
            Controls.Add(label4);
            Controls.Add(txtUsername);
            Controls.Add(label3);
            Controls.Add(txtTenNhanVien);
            Controls.Add(label2);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmChiTietNhanVien";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FrmChiTietNhanVien";
            guna2Panel1.ResumeLayout(false);
            guna2Panel1.PerformLayout();
            guna2Panel2.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Label lblTieuDe;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Button btnBoQua;
        private Guna.UI2.WinForms.Guna2Button btnDongY;
        private Guna.UI2.WinForms.Guna2TextBox txtPassword;
        private Label label4;
        private Guna.UI2.WinForms.Guna2TextBox txtUsername;
        private Label label3;
        private Guna.UI2.WinForms.Guna2TextBox txtTenNhanVien;
        private Label label2;
        private Guna.UI2.WinForms.Guna2CheckBox checkboxNV;
    }
}