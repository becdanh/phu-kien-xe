﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmChiTietNhanVien : Form
    {
        UserDTO userDTO;
        public FrmChiTietNhanVien(UserDTO userDTO = null)
        {
            InitializeComponent();
            this.userDTO = userDTO;
            if (userDTO != null)
            {
                //Cap nhat
                lblTieuDe.Text = "CẬP NHẬT NHÂN VIÊN";
                txtTenNhanVien.Text = userDTO.FullName;
                txtUsername.Text = userDTO.Username;
                txtPassword.Text = userDTO.Password;
                checkboxNV.Checked = userDTO.IsAdmin;
            }
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            using (var db = new PhuKienDB())
            {
                // Check if any required field is empty
                if (string.IsNullOrWhiteSpace(txtUsername.Text) ||
                    string.IsNullOrWhiteSpace(txtPassword.Text) ||
                    string.IsNullOrWhiteSpace(txtTenNhanVien.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Check for duplicate username
                if (userDTO == null && db.users.Any(u => u.Username == txtUsername.Text))
                {
                    MessageBox.Show("Tên đăng nhập đã tồn tại. Vui lòng chọn tên đăng nhập khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (userDTO != null)
                {
                    // Check for duplicate username when updating an existing user
                    if (db.users.Any(u => u.Username == txtUsername.Text && u.UserId != userDTO.UserId))
                    {
                        MessageBox.Show("Tên đăng nhập đã tồn tại. Vui lòng chọn tên đăng nhập khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                if (userDTO == null)
                {
                    // Adding new user
                    var newUser = new Models.User
                    {
                        Username = txtUsername.Text,
                        Password = txtPassword.Text,
                        FullName = txtTenNhanVien.Text,
                        IsAdmin = checkboxNV.Checked
                    };

                    db.users.Add(newUser);
                    db.SaveChanges();
                    MessageBox.Show("Thêm mới nhân viên thành công.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    // Updating existing user
                    var existingUser = db.users.Where(t => t.Username == userDTO.Username).FirstOrDefault();

                    if (existingUser != null)
                    {
                        existingUser.Username = txtUsername.Text;
                        existingUser.Password = txtPassword.Text;
                        existingUser.FullName = txtTenNhanVien.Text;
                        existingUser.IsAdmin = checkboxNV.Checked;

                        db.SaveChanges();
                        MessageBox.Show("Cập nhật nhân viên thành công.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Không tìm thấy nhân viên để cập nhật.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
