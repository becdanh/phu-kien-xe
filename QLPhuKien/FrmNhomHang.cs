﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmNhomHang : Form
    {
        public FrmNhomHang()
        {
            InitializeComponent();
        }

        void LoadNhomHangHoa()
        {

            PhuKienDB db = new PhuKienDB();
            var ls = db.nhomHangHoas
                .Select(t => new NhomHangHoaDTO
                {
                    MaNHH = t.MaNHH,
                    TenNHH = t.TenNHH,
                    GhiChu = t.GhiChu,
                }).ToList();
            nhomHangHoaDTOBindingSource.DataSource = ls;

        }

        private void FrmNhomHang_Load(object sender, EventArgs e)
        {
            LoadNhomHangHoa();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var f = new FrmChiTietNhomHang();
            if (f.ShowDialog() == DialogResult.OK)
            {
                LoadNhomHangHoa();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            var sv = nhomHangHoaDTOBindingSource.Current as NhomHangHoaDTO;
            if (sv != null)
            {
                var f = new FrmChiTietNhomHang(sv);
                if (f.ShowDialog() == DialogResult.OK)
                    LoadNhomHangHoa();
            }
        }

        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                var tukhoa = txtTimKiem.Text.ToLower();
                using (var db = new PhuKienDB())
                {
                    var ls = db.nhomHangHoas
                        .Where(t =>
                        (t.TenNHH.ToLower().Contains(tukhoa.ToLower())
                        || t.GhiChu.ToLower() == tukhoa.ToLower()
                    ))
                    .Select(t => new NhomHangHoaDTO
                    {
                        MaNHH = t.MaNHH,
                        TenNHH = t.TenNHH,
                        GhiChu = t.GhiChu,
                    }).ToList();

                    nhomHangHoaDTOBindingSource.DataSource = null;
                    nhomHangHoaDTOBindingSource.DataSource = ls;
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var sv = nhomHangHoaDTOBindingSource.Current as NhomHangHoaDTO;
            if (sv != null)
            {
                // xac nhan xoa
                var rs = MessageBox.Show(
                    "Xác nhận xóa?",
                    "Thông báo",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question
                    );
                if (rs == DialogResult.OK)
                {
                    PhuKienDB db = new PhuKienDB();
                    var obj = db.nhomHangHoas.Where(t => t.MaNHH == sv.MaNHH).FirstOrDefault();
                    if (obj != null)
                    {
                        db.nhomHangHoas.Remove(obj);
                        db.SaveChanges();
                        LoadNhomHangHoa();
                    }
                }
            }
        }
    }
}
