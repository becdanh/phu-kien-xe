﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmNhaCungCap1 : Form
    {
        public FrmNhaCungCap1()
        {
            InitializeComponent();
        }
        void LoadNhaCungCap()
        {

            PhuKienDB db = new PhuKienDB();
            var ls = db.nhaCungCaps
                .Select(t => new NhaCungCapDTO
                {
                    MaNCC = t.MaNCC,
                    TenNCC = t.TenNCC,
                    Email = t.Email,
                    DienThoai = t.DienThoai,
                    DiaChi = t.DiaChi,
                    NgayTao = t.NgayTao,
                    Disabled = t.Disabled,
                }).ToList();
            nhaCungCapDTOBindingSource.DataSource = ls;

        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            var f = new FrmChiTietNhaCungCap();
            if (f.ShowDialog() == DialogResult.OK)
            {
                LoadNhaCungCap();
            }
        }

        private void FrmNhaCungCap1_Load(object sender, EventArgs e)
        {
            LoadNhaCungCap();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var sv = nhaCungCapDTOBindingSource.Current as NhaCungCapDTO;
            if (sv != null)
            {
                // xac nhan xoa
                var rs = MessageBox.Show(
                    "Xác nhận xóa?",
                    "Thông báo",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question
                    );
                if (rs == DialogResult.OK)
                {
                    PhuKienDB db = new PhuKienDB();
                    var obj = db.nhaCungCaps.Where(t => t.MaNCC == sv.MaNCC).FirstOrDefault();
                    if (obj != null)
                    {
                        db.nhaCungCaps.Remove(obj);
                        db.SaveChanges();
                        LoadNhaCungCap();
                    }
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            var sv = nhaCungCapDTOBindingSource.Current as NhaCungCapDTO;
            if (sv != null)
            {
                var f = new FrmChiTietNhaCungCap(sv);
                if (f.ShowDialog() == DialogResult.OK)
                    LoadNhaCungCap();
            }
        }

        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            var tukhoa = txtTimKiem.Text.ToLower();
            using (var db = new PhuKienDB())
            {
                var ls = db.nhaCungCaps
                    .Where(t =>
                    (t.TenNCC.ToLower().Contains(tukhoa.ToLower())
                    || t.Email.ToLower() == tukhoa.ToLower()
                    || t.DienThoai.ToLower() == tukhoa.ToLower()
                    || t.DiaChi.ToLower() == tukhoa.ToLower()
                ))
                .Select(t => new NhaCungCapDTO
                {
                    MaNCC = t.MaNCC,
                    TenNCC = t.TenNCC,
                    Email = t.Email,
                    DienThoai = t.DienThoai,
                    DiaChi = t.DiaChi,
                    NgayTao = t.NgayTao,
                    Disabled = t.Disabled,
                }).ToList();

                nhaCungCapDTOBindingSource.DataSource = null;
                nhaCungCapDTOBindingSource.DataSource = ls;
            }
        }
    }
}
