﻿using Microsoft.Reporting.WinForms;
using QLPhuKien.DTO;
using QLPhuKien.Migrations;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmBanHang : Form
    {
        private uc_Product productControl;
        List<ChiTietHoaDonDTO> chitiethoadonDTO = new List<ChiTietHoaDonDTO>();
        private int HoaDonId;
        public FrmBanHang()
        {
            InitializeComponent();
            productControl = new uc_Product();
            productControl.ProductClicked += UcProduct_Clicked;
            LoadNhomHangButtons();
            LoadAllProducts();
            LoadKhachHang();
        }
        void LoadKhachHang()
        {
            PhuKienDB db = new PhuKienDB();
            var ls = db.khachHangs.Select(e => new KhachHangDTO
            {
                CustomerId = e.CustomerId,
                FullName = e.FullName,
                SDT = e.SDT,
            }).ToList();
            // day len gd
            cbbKhachHang.DataSource = ls;
            cbbKhachHang.DisplayMember = "FullName";
        }

        private void LoadAllProducts()
        {
            List<HangHoaDTO> danhSachHangHoa = GetDanhSachHangHoa();

            foreach (var hangHoa in danhSachHangHoa)
            {
                uc_Product productItem = new uc_Product();
                productItem.ProductClicked += UcProduct_Clicked;
                productItem.DisplayProductInformation(hangHoa);
                PanelProduct.Controls.Add(productItem);
            }
        }

        private List<HangHoaDTO> GetDanhSachHangHoa()
        {
            using (var db = new PhuKienDB())
            {
                var danhSachHangHoa = db.hangHoas
                    .Select(h => new HangHoaDTO
                    {
                        Barcode = h.Barcode,
                        TenHangHoa = h.TenHangHoa,
                        DonVi = h.DonVi,
                        GiaBan = h.GiaBan,
                        SoLuong = h.SoLuong,
                        MaNHH = h.MaNHH,
                        MaNCC = h.MaNCC,
                        Anh = h.Anh,
                        Disabled = h.Disabled
                    })
                    .ToList();

                return danhSachHangHoa;
            }
        }

        private void LoadNhomHangButtons()
        {
            List<NhomHangHoaDTO> danhSachNhomHang = GetDanhSachNhomHang();

            foreach (var nhomHang in danhSachNhomHang)
            {
                Guna.UI2.WinForms.Guna2Button btn = new Guna.UI2.WinForms.Guna2Button
                {
                    FillColor = Color.FromArgb(50, 55, 89),
                    Size = new Size(172, 61),
                    Text = nhomHang.TenNHH,
                    Tag = nhomHang.MaNHH
                };

                btn.Click += NhomHangButtonClick;

                panelDanhMuc.Controls.Add(btn);
            }

            Guna.UI2.WinForms.Guna2Button btnAllProducts = new Guna.UI2.WinForms.Guna2Button
            {
                FillColor = Color.FromArgb(50, 55, 89),
                Size = new Size(172, 61),
                Text = "All Products",
                Tag = "AllProducts"
            };

            btnAllProducts.Click += NhomHangButtonClick;

            panelDanhMuc.Controls.Add(btnAllProducts);
        }

        private List<NhomHangHoaDTO> GetDanhSachNhomHang()
        {
            using (var db = new PhuKienDB())
            {
                var danhSachNhomHang = db.nhomHangHoas
                    .Select(t => new NhomHangHoaDTO
                    {
                        MaNHH = t.MaNHH,
                        TenNHH = t.TenNHH,
                        GhiChu = t.GhiChu
                    })
                    .ToList();

                return danhSachNhomHang;
            }
        }

        private void NhomHangButtonClick(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2Button clickedButton = sender as Guna.UI2.WinForms.Guna2Button;
            string maNhomHang = clickedButton.Tag.ToString();

            List<HangHoaDTO> productsInCategory;

            if (maNhomHang == "AllProducts")
            {
                productsInCategory = GetDanhSachHangHoa();
            }
            else
            {
                productsInCategory = GetProductsByCategory(maNhomHang);
            }

            PanelProduct.Controls.Clear();

            if (productsInCategory.Count > 0)
            {
                foreach (var product in productsInCategory)
                {
                    uc_Product productItem = new uc_Product();
                    productItem.DisplayProductInformation(product);
                    productItem.ProductClicked += UcProduct_Clicked;
                    PanelProduct.Controls.Add(productItem);
                }
            }
            else
            {
                MessageBox.Show("No products found for the selected category.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private List<HangHoaDTO> GetProductsByCategory(string maNhomHang)
        {
            using (var db = new PhuKienDB())
            {
                var productsInCategory = db.hangHoas
                    .Where(h => h.MaNHH == maNhomHang)
                    .Select(h => new HangHoaDTO
                    {
                        Barcode = h.Barcode,
                        TenHangHoa = h.TenHangHoa,
                        DonVi = h.DonVi,
                        GiaBan = h.GiaBan,
                        SoLuong = h.SoLuong,
                        MaNHH = h.MaNHH,
                        MaNCC = h.MaNCC,
                        Anh = h.Anh,
                        Disabled = h.Disabled
                    })
                    .ToList();

                return productsInCategory;
            }
        }
        private void UpdateTotalAmount()
        {
            decimal totalAmount = chitiethoadonDTO.Sum(item => item.ThanhTien);
            lblTotalAmount.Text = $"Tổng tiền: {totalAmount.ToString("C")}";
        }
        private void UcProduct_Clicked(object sender, ProductClickedEventArgs e)
        {
            HangHoaDTO productInfo = e.ProductInfo;

            var existingItem = chitiethoadonDTO.FirstOrDefault(item => item.Barcode == productInfo.Barcode);

            if (existingItem != null)
            {
                // Kiểm tra xem có đủ hàng trong kho để tăng số lượng mua không
                if (CheckQuantityInStock(productInfo, existingItem.SoLuongMua + 1))
                {
                    existingItem.SoLuongMua++;
                }
                else
                {
                    MessageBox.Show("Số lượng hàng trong kho không đủ.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                // Kiểm tra xem có đủ hàng trong kho để thêm mới vào danh sách không
                if (CheckQuantityInStock(productInfo, 1))
                {
                    ChiTietHoaDonDTO chiTietHoaDon = new ChiTietHoaDonDTO
                    {
                        Barcode = productInfo.Barcode,
                        TenHH = productInfo.TenHangHoa,
                        GiaBan = productInfo.GiaBan,
                        SoLuongMua = 1
                    };

                    chitiethoadonDTO.Add(chiTietHoaDon);
                }
                else
                {
                    MessageBox.Show("Số lượng hàng trong kho không đủ.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            UpdateDataGridView();
            UpdateTotalAmount();
        }

        private bool CheckQuantityInStock(HangHoaDTO productInfo, int quantityToCheck)
        {
            using (var db = new PhuKienDB())
            {
                var productInStock = db.hangHoas.FirstOrDefault(h => h.Barcode == productInfo.Barcode);

                if (productInStock != null && productInStock.SoLuong >= quantityToCheck)
                {
                    return true; // Đủ hàng trong kho
                }

                return false; // Không đủ hàng trong kho
            }
        }

        void UpdateDataGridView()
        {
            chitiethoadonDTO = chitiethoadonDTO.Where(item => item.SoLuongMua > 0).ToList();
            dtgChiTietHoaDon.DataSource = null;
            dtgChiTietHoaDon.DataSource = chitiethoadonDTO;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBan_Click(object sender, EventArgs e)
        {
            // Check if there are products in the shopping cart
            if (chitiethoadonDTO.Count > 0)
            {
                using (var db = new PhuKienDB())
                {
                    // Create a new HoaDon (Invoice) entity
                    HoaDon hoaDon = new HoaDon
                    {
                        NgayTao = DateTime.Now,
                        UserId = FrmDangNhap.LoggedInUser.UserId,
                        CustomerId = ((KhachHangDTO)cbbKhachHang.SelectedItem).CustomerId,
                        TongTien = chitiethoadonDTO.Sum(item => item.ThanhTien),
                    };

                    // Add the HoaDon entity to the database
                    db.hoaDons.Add(hoaDon);
                    db.SaveChanges();

                    // Get the ID of the newly added HoaDon
                    HoaDonId = hoaDon.HoaDonId;

                    // Add ChiTietHoaDon (Invoice Details) to the database
                    foreach (var chiTiet in chitiethoadonDTO)
                    {
                        ChiTietHoaDon chiTietHoaDon = new ChiTietHoaDon
                        {
                            HoaDonId = HoaDonId,
                            Barcode = chiTiet.Barcode,
                            SoLuongMua = chiTiet.SoLuongMua,
                            GiaBan = chiTiet.GiaBan,
                            ThanhTien = chiTiet.ThanhTien,
                        };

                        // Add ChiTietHoaDon entity to the database
                        db.chiTietHoaDons.Add(chiTietHoaDon);

                        // Update the quantity of the product in stock
                        var product = db.hangHoas.FirstOrDefault(h => h.Barcode == chiTiet.Barcode);
                        if (product != null)
                        {
                            product.SoLuong -= chiTiet.SoLuongMua;
                        }
                    }

                    // Save changes to the database
                    db.SaveChanges();
                }

                // Display a success message
                MessageBox.Show("Bán hàng thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Clear the shopping cart after selling
                chitiethoadonDTO.Clear();
                UpdateDataGridView();
                UpdateTotalAmount();
            }
            else
            {
                // Display a warning if there are no products in the shopping cart
                MessageBox.Show("Không có sản phẩm để bán.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dtgChiTietHoaDon_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dtgChiTietHoaDon.Rows.Count)
            {
                // Get the selected ChiTietHoaDon item
                ChiTietHoaDonDTO selectedChiTiet = (ChiTietHoaDonDTO)dtgChiTietHoaDon.Rows[e.RowIndex].DataBoundItem;

                // Get the current stock quantity for the selected product
                int currentStockQuantity = GetCurrentStockQuantity(selectedChiTiet.Barcode);

                // Open the FrmSoLuong form with the selected ChiTietHoaDon item and maximum allowed quantity
                FrmSoLuong frmSoLuong = new FrmSoLuong(selectedChiTiet.SoLuongMua, currentStockQuantity);
                frmSoLuong.ShowDialog();

                // Update the quantity in the shopping cart after returning from FrmSoLuong
                selectedChiTiet.SoLuongMua = frmSoLuong.SelectedQuantity;
                UpdateDataGridView();
                UpdateTotalAmount();
            }
        }

        private int GetCurrentStockQuantity(string barcode)
        {
            using (var db = new PhuKienDB())
            {
                var productInStock = db.hangHoas.FirstOrDefault(h => h.Barcode == barcode);

                if (productInStock != null)
                {
                    return productInStock.SoLuong;
                }

                return 0; // Handle the case where the product is not found
            }
        }

        private void btnInHD_Click(object sender, EventArgs e)
        {
            if (chitiethoadonDTO.Count > 0)
            {
                using (var db = new PhuKienDB())
                {
                    // Create a new HoaDon (Invoice) entity
                    HoaDon hoaDon = new HoaDon
                    {
                        NgayTao = DateTime.Now,
                        UserId = FrmDangNhap.LoggedInUser.UserId,
                        CustomerId = ((KhachHangDTO)cbbKhachHang.SelectedItem).CustomerId,
                        TongTien = chitiethoadonDTO.Sum(item => item.ThanhTien),
                    };

                    // Add the HoaDon entity to the database
                    db.hoaDons.Add(hoaDon);
                    db.SaveChanges();

                    // Get the ID of the newly added HoaDon
                    HoaDonId = hoaDon.HoaDonId;

                    // Add ChiTietHoaDon (Invoice Details) to the database
                    foreach (var chiTiet in chitiethoadonDTO)
                    {
                        ChiTietHoaDon chiTietHoaDon = new ChiTietHoaDon
                        {
                            HoaDonId = HoaDonId,
                            Barcode = chiTiet.Barcode,
                            SoLuongMua = chiTiet.SoLuongMua,
                            GiaBan = chiTiet.GiaBan,
                            ThanhTien = chiTiet.ThanhTien,
                        };

                        // Add ChiTietHoaDon entity to the database
                        db.chiTietHoaDons.Add(chiTietHoaDon);

                        // Update the quantity of the product in stock
                        var product = db.hangHoas.FirstOrDefault(h => h.Barcode == chiTiet.Barcode);
                        if (product != null)
                        {
                            product.SoLuong -= chiTiet.SoLuongMua;
                        }
                    }

                    // Save changes to the database
                    db.SaveChanges();
                }

                // Display a success message
                MessageBox.Show("Bán hàng thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Open the HelloForm and pass the HoaDonId
                HelloForm helloForm = new HelloForm(HoaDonId);
                helloForm.ShowDialog();

                // Clear the shopping cart after selling
                chitiethoadonDTO.Clear();
                UpdateDataGridView();
                UpdateTotalAmount();
            }
            else
            {
                // Display a warning if there are no products in the shopping cart
                MessageBox.Show("Không có sản phẩm để bán.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                PerformSearch();
            }
        }
        private void PerformSearch()
        {
            string keyword = txtTimKiem.Text.Trim().ToLower();

            if (!string.IsNullOrEmpty(keyword))
            {
                List<HangHoaDTO> filteredProducts = SearchProducts(keyword);

                if (filteredProducts.Count > 0)
                {
                    // Hiển thị các UserControl của hàng hóa tìm được
                    PanelProduct.Controls.Clear();

                    foreach (var product in filteredProducts)
                    {
                        uc_Product productItem = new uc_Product();
                        productItem.DisplayProductInformation(product);
                        productItem.ProductClicked += UcProduct_Clicked;
                        PanelProduct.Controls.Add(productItem);
                    }
                }
                else
                {
                    MessageBox.Show("Không tìm thấy sản phẩm nào.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập từ khóa tìm kiếm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private List<HangHoaDTO> SearchProducts(string keyword)
        {
            using (var db = new PhuKienDB())
            {
                var searchResult = db.hangHoas
                    .Where(h => h.TenHangHoa.ToLower().Contains(keyword) ||
                                h.Barcode.ToLower().Contains(keyword))
                    .Select(h => new HangHoaDTO
                    {
                        Barcode = h.Barcode,
                        TenHangHoa = h.TenHangHoa,
                        DonVi = h.DonVi,
                        GiaBan = h.GiaBan,
                        SoLuong = h.SoLuong,
                        MaNHH = h.MaNHH,
                        MaNCC = h.MaNCC,
                        Anh = h.Anh,
                        Disabled = h.Disabled
                    })
                    .ToList();

                return searchResult;
            }
        }

        private void cbbKhachHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbKhachHang.SelectedItem != null)
            {
                KhachHangDTO selectedCustomer = (KhachHangDTO)cbbKhachHang.SelectedItem;
                txtSDT.Text = "Số điện thoại: " + selectedCustomer.SDT;
            }
            else
            {
                txtSDT.Text = string.Empty;
            }
        }
    }
}
