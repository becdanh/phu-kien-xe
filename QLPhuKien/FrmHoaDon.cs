﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmHoaDon : Form
    {
        public FrmHoaDon()
        {
            InitializeComponent();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {

        }

        void LoadHoaDon()
        {
            using (PhuKienDB db = new PhuKienDB())
            {
                var hoaDonList = db.hoaDons
                    .Select(hoaDon => new HoaDonDTO
                    {
                        HoaDonId = hoaDon.HoaDonId,
                        NgayTao = hoaDon.NgayTao,
                        UserId = hoaDon.UserId,
                        TongTien = hoaDon.TongTien,
                        TenNhanVien = hoaDon.User.FullName,
                        TenKhachHang = hoaDon.KhachHang.FullName
                    })
                    .ToList();
                hoaDonDTOBindingSource.DataSource = hoaDonList;
            }
        }

        private void FrmHoaDon_Load(object sender, EventArgs e)
        {
            LoadHoaDon();
        }

        private void btnXemChiTiet_Click(object sender, EventArgs e)
        {
            if (dtgHoaDon.SelectedRows.Count > 0)
            {
                // Get the selected HoaDonDTO
                HoaDonDTO selectedHoaDon = (HoaDonDTO)dtgHoaDon.SelectedRows[0].DataBoundItem;

                // Open the detail form with the selected HoaDonDTO
                FrmChiTietHoaDon frmChiTietHoaDon = new FrmChiTietHoaDon(selectedHoaDon);
                frmChiTietHoaDon.ShowDialog();
            }
        }

        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                var keyword = txtTimKiem.Text.ToLower();

                using (var db = new PhuKienDB())
                {
                    var hoaDonList = db.hoaDons
                        .Where(hoaDon =>
                            hoaDon.NgayTao.ToString().ToLower().Contains(keyword) ||
                            hoaDon.User.FullName.ToLower().Contains(keyword) ||
                            hoaDon.KhachHang.FullName.ToLower().Contains(keyword))
                        .Select(hoaDon => new HoaDonDTO
                        {
                            HoaDonId = hoaDon.HoaDonId,
                            NgayTao = hoaDon.NgayTao,
                            UserId = hoaDon.UserId,
                            TongTien = hoaDon.TongTien,
                            TenNhanVien = hoaDon.User.FullName,
                            TenKhachHang = hoaDon.KhachHang.FullName
                        })
                        .ToList();

                    hoaDonDTOBindingSource.DataSource = hoaDonList;
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var hoaDon = hoaDonDTOBindingSource.Current as HoaDonDTO;

            if (hoaDon != null)
            {
                // Confirm deletion
                var result = MessageBox.Show(
                    "Xác nhận xóa?",
                    "Thông báo",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question
                );

                if (result == DialogResult.OK)
                {
                    using (PhuKienDB db = new PhuKienDB())
                    {
                        var obj = db.hoaDons.Where(hd => hd.HoaDonId == hoaDon.HoaDonId).FirstOrDefault();

                        if (obj != null)
                        {
                            db.hoaDons.Remove(obj);
                            db.SaveChanges();
                            LoadHoaDon();
                        }
                    }
                }
            }
        }
    }
}
