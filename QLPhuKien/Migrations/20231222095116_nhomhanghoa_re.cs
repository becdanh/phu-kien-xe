﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class nhomhanghoa_re : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NhomHangHoa",
                table: "NhomHangHoa");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "NhomHangHoa");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NhomHangHoa",
                table: "NhomHangHoa",
                column: "MaNHH");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NhomHangHoa",
                table: "NhomHangHoa");

            migrationBuilder.AddColumn<int>(
                name: "ID",
                table: "NhomHangHoa",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NhomHangHoa",
                table: "NhomHangHoa",
                column: "ID");
        }
    }
}
