﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class Hoadon : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "HoaDon",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_HoaDon_CustomerId",
                table: "HoaDon",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDon_KhachHang_CustomerId",
                table: "HoaDon",
                column: "CustomerId",
                principalTable: "KhachHang",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HoaDon_KhachHang_CustomerId",
                table: "HoaDon");

            migrationBuilder.DropIndex(
                name: "IX_HoaDon_CustomerId",
                table: "HoaDon");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "HoaDon");
        }
    }
}
