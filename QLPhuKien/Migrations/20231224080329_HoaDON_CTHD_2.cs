﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class HoaDON_CTHD_2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DonGia",
                table: "ChiTietHoaDon");

            migrationBuilder.RenameColumn(
                name: "SoLuong",
                table: "ChiTietHoaDon",
                newName: "SoLuongMua");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SoLuongMua",
                table: "ChiTietHoaDon",
                newName: "SoLuong");

            migrationBuilder.AddColumn<decimal>(
                name: "DonGia",
                table: "ChiTietHoaDon",
                type: "TEXT",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
