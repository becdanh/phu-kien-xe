﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class updatedatabase_hanghoa : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "MaNHH",
                table: "HangHoa",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "MaNHH",
                table: "HangHoa",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT");
        }
    }
}
