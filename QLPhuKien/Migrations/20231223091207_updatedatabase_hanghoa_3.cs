﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class updatedatabase_hanghoa_3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HangHoa_NhomHangHoa_NhomHangHoaMaNHH",
                table: "HangHoa");

            migrationBuilder.DropIndex(
                name: "IX_HangHoa_NhomHangHoaMaNHH",
                table: "HangHoa");

            migrationBuilder.DropColumn(
                name: "NhomHangHoaMaNHH",
                table: "HangHoa");

            migrationBuilder.CreateIndex(
                name: "IX_HangHoa_MaNHH",
                table: "HangHoa",
                column: "MaNHH");

            migrationBuilder.AddForeignKey(
                name: "FK_HangHoa_NhomHangHoa_MaNHH",
                table: "HangHoa",
                column: "MaNHH",
                principalTable: "NhomHangHoa",
                principalColumn: "MaNHH",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HangHoa_NhomHangHoa_MaNHH",
                table: "HangHoa");

            migrationBuilder.DropIndex(
                name: "IX_HangHoa_MaNHH",
                table: "HangHoa");

            migrationBuilder.AddColumn<string>(
                name: "NhomHangHoaMaNHH",
                table: "HangHoa",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_HangHoa_NhomHangHoaMaNHH",
                table: "HangHoa",
                column: "NhomHangHoaMaNHH");

            migrationBuilder.AddForeignKey(
                name: "FK_HangHoa_NhomHangHoa_NhomHangHoaMaNHH",
                table: "HangHoa",
                column: "NhomHangHoaMaNHH",
                principalTable: "NhomHangHoa",
                principalColumn: "MaNHH",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
