﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class updatedatabase_hanghoa_4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MaNCC",
                table: "HangHoa",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_HangHoa_MaNCC",
                table: "HangHoa",
                column: "MaNCC");

            migrationBuilder.AddForeignKey(
                name: "FK_HangHoa_NhaCungCap_MaNCC",
                table: "HangHoa",
                column: "MaNCC",
                principalTable: "NhaCungCap",
                principalColumn: "MaNCC",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HangHoa_NhaCungCap_MaNCC",
                table: "HangHoa");

            migrationBuilder.DropIndex(
                name: "IX_HangHoa_MaNCC",
                table: "HangHoa");

            migrationBuilder.DropColumn(
                name: "MaNCC",
                table: "HangHoa");
        }
    }
}
