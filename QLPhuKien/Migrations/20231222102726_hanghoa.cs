﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QLPhuKien.Migrations
{
    /// <inheritdoc />
    public partial class hanghoa : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HangHoa",
                columns: table => new
                {
                    Barcode = table.Column<string>(type: "TEXT", maxLength: 13, nullable: false),
                    TenHangHoa = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false),
                    TenTat = table.Column<string>(type: "TEXT", maxLength: 20, nullable: false),
                    DonGia = table.Column<decimal>(type: "TEXT", nullable: false),
                    MaNHH = table.Column<int>(type: "INTEGER", nullable: false),
                    MoTa = table.Column<string>(type: "TEXT", maxLength: 255, nullable: false),
                    Disabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    NhomHangHoaMaNHH = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HangHoa", x => x.Barcode);
                    table.ForeignKey(
                        name: "FK_HangHoa_NhomHangHoa_NhomHangHoaMaNHH",
                        column: x => x.NhomHangHoaMaNHH,
                        principalTable: "NhomHangHoa",
                        principalColumn: "MaNHH",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HangHoa_NhomHangHoaMaNHH",
                table: "HangHoa",
                column: "NhomHangHoaMaNHH");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HangHoa");
        }
    }
}
