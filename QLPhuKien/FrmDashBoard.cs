﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmDashBoard : Form
    {
        public FrmDashBoard()
        {
            InitializeComponent();
            UpdateClock();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateClock();
        }
        private void UpdateClock()
        {
            // Lấy thời gian hiện tại
            DateTime now = DateTime.Now;

            // Hiển thị ngày tháng năm
            string formattedDate = now.ToString("dd/MM/yyyy");
            // Hiển thị giờ phút giây
            string formattedTime = now.ToString("HH:mm:ss");

            // Hiển thị lên Label hoặc TextBox
            labelClock.Text = formattedDate;
            labelSecond.Text = formattedTime;
        }
    }
}
