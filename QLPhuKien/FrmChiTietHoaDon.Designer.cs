﻿namespace QLPhuKien
{
    partial class FrmChiTietHoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChiTietHoaDon));
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            btnThoat = new Guna.UI2.WinForms.Guna2ImageButton();
            lblTieuDe = new Label();
            guna2DataGridView1 = new Guna.UI2.WinForms.Guna2DataGridView();
            barcodeDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            tenHHDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            soLuongMuaDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            giaBanDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            thanhTienDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            chiTietHoaDonDTOBindingSource = new BindingSource(components);
            lblTenNhanVien = new Label();
            lblTenKhachHang = new Label();
            lblTongTien = new Label();
            lblNgayLapHoaDon = new Label();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            btnInHD = new Guna.UI2.WinForms.Guna2TileButton();
            guna2Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)guna2DataGridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)chiTietHoaDonDTOBindingSource).BeginInit();
            SuspendLayout();
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(btnInHD);
            guna2Panel1.Controls.Add(btnThoat);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges4;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Margin = new Padding(3, 2, 3, 2);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges5;
            guna2Panel1.Size = new Size(1015, 118);
            guna2Panel1.TabIndex = 54;
            // 
            // btnThoat
            // 
            btnThoat.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            btnThoat.CheckedState.ImageSize = new Size(64, 64);
            btnThoat.HoverState.ImageSize = new Size(64, 64);
            btnThoat.Image = (Image)resources.GetObject("btnThoat.Image");
            btnThoat.ImageOffset = new Point(0, 0);
            btnThoat.ImageRotate = 0F;
            btnThoat.ImageSize = new Size(40, 40);
            btnThoat.Location = new Point(944, 36);
            btnThoat.Name = "btnThoat";
            btnThoat.PressedState.ImageSize = new Size(64, 64);
            btnThoat.ShadowDecoration.CustomizableEdges = customizableEdges3;
            btnThoat.Size = new Size(47, 45);
            btnThoat.TabIndex = 65;
            btnThoat.Click += btnThoat_Click;
            // 
            // lblTieuDe
            // 
            lblTieuDe.AutoSize = true;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(351, 36);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(273, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "CHI TIẾT HÓA ĐƠN";
            // 
            // guna2DataGridView1
            // 
            guna2DataGridView1.AllowUserToAddRows = false;
            guna2DataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = Color.White;
            guna2DataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            guna2DataGridView1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            guna2DataGridView1.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = Color.FromArgb(232, 234, 237);
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = Color.FromArgb(232, 234, 237);
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            guna2DataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            guna2DataGridView1.ColumnHeadersHeight = 40;
            guna2DataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            guna2DataGridView1.Columns.AddRange(new DataGridViewColumn[] { barcodeDataGridViewTextBoxColumn, tenHHDataGridViewTextBoxColumn, soLuongMuaDataGridViewTextBoxColumn, giaBanDataGridViewTextBoxColumn, thanhTienDataGridViewTextBoxColumn });
            guna2DataGridView1.DataSource = chiTietHoaDonDTOBindingSource;
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = Color.White;
            dataGridViewCellStyle3.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = Color.FromArgb(71, 69, 94);
            dataGridViewCellStyle3.SelectionBackColor = Color.FromArgb(239, 241, 243);
            dataGridViewCellStyle3.SelectionForeColor = Color.FromArgb(71, 69, 94);
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
            guna2DataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            guna2DataGridView1.GridColor = Color.FromArgb(231, 229, 255);
            guna2DataGridView1.Location = new Point(23, 258);
            guna2DataGridView1.Margin = new Padding(3, 2, 3, 2);
            guna2DataGridView1.Name = "guna2DataGridView1";
            guna2DataGridView1.ReadOnly = true;
            guna2DataGridView1.RowHeadersVisible = false;
            guna2DataGridView1.RowHeadersWidth = 51;
            guna2DataGridView1.RowTemplate.Height = 29;
            guna2DataGridView1.Size = new Size(968, 328);
            guna2DataGridView1.TabIndex = 56;
            guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.BackColor = Color.White;
            guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.Font = null;
            guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.ForeColor = Color.Empty;
            guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = Color.Empty;
            guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = Color.Empty;
            guna2DataGridView1.ThemeStyle.BackColor = Color.White;
            guna2DataGridView1.ThemeStyle.GridColor = Color.FromArgb(231, 229, 255);
            guna2DataGridView1.ThemeStyle.HeaderStyle.BackColor = Color.FromArgb(100, 88, 255);
            guna2DataGridView1.ThemeStyle.HeaderStyle.BorderStyle = DataGridViewHeaderBorderStyle.None;
            guna2DataGridView1.ThemeStyle.HeaderStyle.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            guna2DataGridView1.ThemeStyle.HeaderStyle.ForeColor = Color.White;
            guna2DataGridView1.ThemeStyle.HeaderStyle.HeaightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            guna2DataGridView1.ThemeStyle.HeaderStyle.Height = 40;
            guna2DataGridView1.ThemeStyle.ReadOnly = true;
            guna2DataGridView1.ThemeStyle.RowsStyle.BackColor = Color.White;
            guna2DataGridView1.ThemeStyle.RowsStyle.BorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            guna2DataGridView1.ThemeStyle.RowsStyle.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            guna2DataGridView1.ThemeStyle.RowsStyle.ForeColor = Color.FromArgb(71, 69, 94);
            guna2DataGridView1.ThemeStyle.RowsStyle.Height = 29;
            guna2DataGridView1.ThemeStyle.RowsStyle.SelectionBackColor = Color.FromArgb(231, 229, 255);
            guna2DataGridView1.ThemeStyle.RowsStyle.SelectionForeColor = Color.FromArgb(71, 69, 94);
            // 
            // barcodeDataGridViewTextBoxColumn
            // 
            barcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode";
            barcodeDataGridViewTextBoxColumn.HeaderText = "Barcode";
            barcodeDataGridViewTextBoxColumn.MinimumWidth = 6;
            barcodeDataGridViewTextBoxColumn.Name = "barcodeDataGridViewTextBoxColumn";
            barcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tenHHDataGridViewTextBoxColumn
            // 
            tenHHDataGridViewTextBoxColumn.DataPropertyName = "TenHH";
            tenHHDataGridViewTextBoxColumn.HeaderText = "Hàng hóa";
            tenHHDataGridViewTextBoxColumn.MinimumWidth = 6;
            tenHHDataGridViewTextBoxColumn.Name = "tenHHDataGridViewTextBoxColumn";
            tenHHDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // soLuongMuaDataGridViewTextBoxColumn
            // 
            soLuongMuaDataGridViewTextBoxColumn.DataPropertyName = "SoLuongMua";
            soLuongMuaDataGridViewTextBoxColumn.HeaderText = "Số lượng mua";
            soLuongMuaDataGridViewTextBoxColumn.MinimumWidth = 6;
            soLuongMuaDataGridViewTextBoxColumn.Name = "soLuongMuaDataGridViewTextBoxColumn";
            soLuongMuaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // giaBanDataGridViewTextBoxColumn
            // 
            giaBanDataGridViewTextBoxColumn.DataPropertyName = "GiaBan";
            giaBanDataGridViewTextBoxColumn.HeaderText = "Giá bán";
            giaBanDataGridViewTextBoxColumn.MinimumWidth = 6;
            giaBanDataGridViewTextBoxColumn.Name = "giaBanDataGridViewTextBoxColumn";
            giaBanDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // thanhTienDataGridViewTextBoxColumn
            // 
            thanhTienDataGridViewTextBoxColumn.DataPropertyName = "ThanhTien";
            thanhTienDataGridViewTextBoxColumn.HeaderText = "Thành tiền";
            thanhTienDataGridViewTextBoxColumn.MinimumWidth = 6;
            thanhTienDataGridViewTextBoxColumn.Name = "thanhTienDataGridViewTextBoxColumn";
            thanhTienDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // chiTietHoaDonDTOBindingSource
            // 
            chiTietHoaDonDTOBindingSource.DataSource = typeof(DTO.ChiTietHoaDonDTO);
            // 
            // lblTenNhanVien
            // 
            lblTenNhanVien.AutoSize = true;
            lblTenNhanVien.Location = new Point(165, 139);
            lblTenNhanVien.Name = "lblTenNhanVien";
            lblTenNhanVien.Size = new Size(75, 20);
            lblTenNhanVien.TabIndex = 57;
            lblTenNhanVien.Text = "Nhân viên";
            // 
            // lblTenKhachHang
            // 
            lblTenKhachHang.AutoSize = true;
            lblTenKhachHang.Location = new Point(165, 178);
            lblTenKhachHang.Name = "lblTenKhachHang";
            lblTenKhachHang.Size = new Size(86, 20);
            lblTenKhachHang.TabIndex = 58;
            lblTenKhachHang.Text = "Khách hàng";
            // 
            // lblTongTien
            // 
            lblTongTien.AutoSize = true;
            lblTongTien.Location = new Point(473, 219);
            lblTongTien.Name = "lblTongTien";
            lblTongTien.Size = new Size(72, 20);
            lblTongTien.TabIndex = 59;
            lblTongTien.Text = "Tổng tiền";
            // 
            // lblNgayLapHoaDon
            // 
            lblNgayLapHoaDon.AutoSize = true;
            lblNgayLapHoaDon.Location = new Point(165, 219);
            lblNgayLapHoaDon.Name = "lblNgayLapHoaDon";
            lblNgayLapHoaDon.Size = new Size(128, 20);
            lblNgayLapHoaDon.TabIndex = 60;
            lblNgayLapHoaDon.Text = "Ngày lập hóa đơn";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(23, 219);
            label1.Name = "label1";
            label1.Size = new Size(136, 20);
            label1.TabIndex = 63;
            label1.Text = "Ngày lập hóa đơn:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(23, 178);
            label2.Name = "label2";
            label2.Size = new Size(95, 20);
            label2.TabIndex = 62;
            label2.Text = "Khách hàng:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(23, 139);
            label3.Name = "label3";
            label3.Size = new Size(84, 20);
            label3.TabIndex = 61;
            label3.Text = "Nhân viên:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(391, 219);
            label4.Name = "label4";
            label4.Size = new Size(80, 20);
            label4.TabIndex = 64;
            label4.Text = "Tổng tiền:";
            // 
            // btnInHD
            // 
            btnInHD.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            btnInHD.BorderRadius = 10;
            btnInHD.CustomizableEdges = customizableEdges1;
            btnInHD.DisabledState.BorderColor = Color.DarkGray;
            btnInHD.DisabledState.CustomBorderColor = Color.DarkGray;
            btnInHD.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnInHD.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnInHD.FillColor = Color.FromArgb(254, 205, 220);
            btnInHD.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnInHD.ForeColor = Color.Black;
            btnInHD.Image = (Image)resources.GetObject("btnInHD.Image");
            btnInHD.ImageSize = new Size(30, 30);
            btnInHD.Location = new Point(843, 18);
            btnInHD.Name = "btnInHD";
            btnInHD.ShadowDecoration.CustomizableEdges = customizableEdges2;
            btnInHD.Size = new Size(84, 82);
            btnInHD.TabIndex = 66;
            btnInHD.Text = "In HĐ";
            btnInHD.Click += btnInHD_Click;
            // 
            // FrmChiTietHoaDon
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1015, 611);
            Controls.Add(label4);
            Controls.Add(label1);
            Controls.Add(label2);
            Controls.Add(label3);
            Controls.Add(lblNgayLapHoaDon);
            Controls.Add(lblTongTien);
            Controls.Add(lblTenKhachHang);
            Controls.Add(lblTenNhanVien);
            Controls.Add(guna2DataGridView1);
            Controls.Add(guna2Panel1);
            FormBorderStyle = FormBorderStyle.None;
            Margin = new Padding(3, 2, 3, 2);
            Name = "FrmChiTietHoaDon";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FrmChiTietHoaDon";
            guna2Panel1.ResumeLayout(false);
            guna2Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)guna2DataGridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)chiTietHoaDonDTOBindingSource).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Label lblTieuDe;
        private Guna.UI2.WinForms.Guna2DataGridView guna2DataGridView1;
        private BindingSource chiTietHoaDonDTOBindingSource;
        private DataGridViewTextBoxColumn barcodeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn tenHHDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn soLuongMuaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn giaBanDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn thanhTienDataGridViewTextBoxColumn;
        private Label lblTenNhanVien;
        private Label lblTenKhachHang;
        private Label lblTongTien;
        private Label lblNgayLapHoaDon;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Guna.UI2.WinForms.Guna2ImageButton btnThoat;
        private Guna.UI2.WinForms.Guna2TileButton btnInHD;
    }
}