﻿namespace QLPhuKien
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges6 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges7 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges8 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges9 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges10 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges17 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges18 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges11 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges12 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges13 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges14 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges15 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges16 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges27 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges28 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges19 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges20 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges21 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges22 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges23 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges24 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges25 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges26 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges29 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges30 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            btnHangHoa = new Guna.UI2.WinForms.Guna2Button();
            btnHoaDon = new Guna.UI2.WinForms.Guna2Button();
            btnNhaCungCap = new Guna.UI2.WinForms.Guna2Button();
            btnNhomHangHoa = new Guna.UI2.WinForms.Guna2Button();
            btnOverview = new Guna.UI2.WinForms.Guna2Button();
            pictureBox1 = new PictureBox();
            guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(components);
            guna2Panel_top = new Guna.UI2.WinForms.Guna2Panel();
            guna2ControlBox3 = new Guna.UI2.WinForms.Guna2ControlBox();
            guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            label_val = new Label();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            btnNhanVien = new Guna.UI2.WinForms.Guna2Button();
            btnBanHang = new Guna.UI2.WinForms.Guna2Button();
            btnDangXuat = new Guna.UI2.WinForms.Guna2Button();
            btnKhachHang = new Guna.UI2.WinForms.Guna2Button();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            guna2Panel_top.SuspendLayout();
            guna2Panel1.SuspendLayout();
            SuspendLayout();
            // 
            // btnHangHoa
            // 
            btnHangHoa.CustomizableEdges = customizableEdges1;
            btnHangHoa.DisabledState.BorderColor = Color.DarkGray;
            btnHangHoa.DisabledState.CustomBorderColor = Color.DarkGray;
            btnHangHoa.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnHangHoa.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnHangHoa.FillColor = Color.Transparent;
            btnHangHoa.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnHangHoa.ForeColor = Color.Black;
            btnHangHoa.HoverState.BorderColor = Color.Navy;
            btnHangHoa.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnHangHoa.HoverState.ForeColor = Color.White;
            btnHangHoa.Image = (Image)resources.GetObject("btnHangHoa.Image");
            btnHangHoa.ImageAlign = HorizontalAlignment.Left;
            btnHangHoa.ImageOffset = new Point(8, 0);
            btnHangHoa.ImageSize = new Size(30, 30);
            btnHangHoa.ImeMode = ImeMode.On;
            btnHangHoa.Location = new Point(1, 322);
            btnHangHoa.Name = "btnHangHoa";
            btnHangHoa.ShadowDecoration.CustomizableEdges = customizableEdges2;
            btnHangHoa.Size = new Size(248, 60);
            btnHangHoa.TabIndex = 6;
            btnHangHoa.Text = "Hàng hóa";
            btnHangHoa.Click += btnHangHoa_Click;
            // 
            // btnHoaDon
            // 
            btnHoaDon.CustomizableEdges = customizableEdges3;
            btnHoaDon.DisabledState.BorderColor = Color.DarkGray;
            btnHoaDon.DisabledState.CustomBorderColor = Color.DarkGray;
            btnHoaDon.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnHoaDon.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnHoaDon.FillColor = Color.Transparent;
            btnHoaDon.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnHoaDon.ForeColor = Color.Black;
            btnHoaDon.HoverState.BorderColor = Color.Navy;
            btnHoaDon.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnHoaDon.HoverState.ForeColor = Color.White;
            btnHoaDon.Image = (Image)resources.GetObject("btnHoaDon.Image");
            btnHoaDon.ImageAlign = HorizontalAlignment.Left;
            btnHoaDon.ImageOffset = new Point(8, 0);
            btnHoaDon.ImageSize = new Size(30, 30);
            btnHoaDon.ImeMode = ImeMode.On;
            btnHoaDon.Location = new Point(1, 387);
            btnHoaDon.Name = "btnHoaDon";
            btnHoaDon.ShadowDecoration.CustomizableEdges = customizableEdges4;
            btnHoaDon.Size = new Size(248, 60);
            btnHoaDon.TabIndex = 4;
            btnHoaDon.Text = "Hóa đơn";
            btnHoaDon.Click += btnHoaDon_Click;
            // 
            // btnNhaCungCap
            // 
            btnNhaCungCap.CustomizableEdges = customizableEdges5;
            btnNhaCungCap.DisabledState.BorderColor = Color.DarkGray;
            btnNhaCungCap.DisabledState.CustomBorderColor = Color.DarkGray;
            btnNhaCungCap.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnNhaCungCap.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnNhaCungCap.FillColor = Color.Transparent;
            btnNhaCungCap.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnNhaCungCap.ForeColor = Color.Black;
            btnNhaCungCap.HoverState.BorderColor = Color.Navy;
            btnNhaCungCap.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnNhaCungCap.HoverState.ForeColor = Color.White;
            btnNhaCungCap.Image = (Image)resources.GetObject("btnNhaCungCap.Image");
            btnNhaCungCap.ImageAlign = HorizontalAlignment.Left;
            btnNhaCungCap.ImageOffset = new Point(8, 0);
            btnNhaCungCap.ImageSize = new Size(30, 30);
            btnNhaCungCap.ImeMode = ImeMode.On;
            btnNhaCungCap.Location = new Point(1, 192);
            btnNhaCungCap.Name = "btnNhaCungCap";
            btnNhaCungCap.ShadowDecoration.CustomizableEdges = customizableEdges6;
            btnNhaCungCap.Size = new Size(248, 60);
            btnNhaCungCap.TabIndex = 3;
            btnNhaCungCap.Text = "Nhà cung cấp";
            btnNhaCungCap.Click += btnNhaCungCap_Click;
            // 
            // btnNhomHangHoa
            // 
            btnNhomHangHoa.CustomizableEdges = customizableEdges7;
            btnNhomHangHoa.DisabledState.BorderColor = Color.DarkGray;
            btnNhomHangHoa.DisabledState.CustomBorderColor = Color.DarkGray;
            btnNhomHangHoa.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnNhomHangHoa.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnNhomHangHoa.FillColor = Color.Transparent;
            btnNhomHangHoa.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnNhomHangHoa.ForeColor = Color.Black;
            btnNhomHangHoa.HoverState.BorderColor = Color.Navy;
            btnNhomHangHoa.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnNhomHangHoa.HoverState.ForeColor = Color.White;
            btnNhomHangHoa.Image = (Image)resources.GetObject("btnNhomHangHoa.Image");
            btnNhomHangHoa.ImageAlign = HorizontalAlignment.Left;
            btnNhomHangHoa.ImageOffset = new Point(8, 0);
            btnNhomHangHoa.ImageSize = new Size(30, 30);
            btnNhomHangHoa.ImeMode = ImeMode.On;
            btnNhomHangHoa.Location = new Point(1, 257);
            btnNhomHangHoa.Name = "btnNhomHangHoa";
            btnNhomHangHoa.ShadowDecoration.CustomizableEdges = customizableEdges8;
            btnNhomHangHoa.Size = new Size(248, 60);
            btnNhomHangHoa.TabIndex = 2;
            btnNhomHangHoa.Text = "Nhóm hàng hóa";
            btnNhomHangHoa.Click += btnNhomHangHoa_Click;
            // 
            // btnOverview
            // 
            btnOverview.CustomizableEdges = customizableEdges9;
            btnOverview.DisabledState.BorderColor = Color.DarkGray;
            btnOverview.DisabledState.CustomBorderColor = Color.DarkGray;
            btnOverview.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnOverview.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnOverview.FillColor = Color.Transparent;
            btnOverview.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnOverview.ForeColor = Color.Black;
            btnOverview.HoverState.BorderColor = Color.Navy;
            btnOverview.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnOverview.HoverState.ForeColor = Color.White;
            btnOverview.Image = Properties.Resources.dashaucun;
            btnOverview.ImageAlign = HorizontalAlignment.Left;
            btnOverview.ImageOffset = new Point(8, 0);
            btnOverview.ImageSize = new Size(30, 30);
            btnOverview.ImeMode = ImeMode.On;
            btnOverview.Location = new Point(1, 127);
            btnOverview.Name = "btnOverview";
            btnOverview.ShadowDecoration.CustomizableEdges = customizableEdges10;
            btnOverview.Size = new Size(248, 60);
            btnOverview.TabIndex = 1;
            btnOverview.Text = "Ô vờ view";
            btnOverview.Click += btnOverview_Click;
            // 
            // pictureBox1
            // 
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(1, 1);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(248, 120);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            // 
            // guna2DragControl1
            // 
            guna2DragControl1.DockIndicatorTransparencyValue = 0.6D;
            guna2DragControl1.TargetControl = guna2Panel_top;
            guna2DragControl1.UseTransparentDrag = true;
            // 
            // guna2Panel_top
            // 
            guna2Panel_top.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel_top.Controls.Add(guna2ControlBox3);
            guna2Panel_top.Controls.Add(guna2ControlBox2);
            guna2Panel_top.Controls.Add(guna2ControlBox1);
            guna2Panel_top.Controls.Add(label_val);
            guna2Panel_top.CustomBorderColor = Color.Black;
            guna2Panel_top.CustomizableEdges = customizableEdges17;
            guna2Panel_top.Dock = DockStyle.Top;
            guna2Panel_top.Location = new Point(250, 0);
            guna2Panel_top.Name = "guna2Panel_top";
            guna2Panel_top.ShadowDecoration.CustomizableEdges = customizableEdges18;
            guna2Panel_top.Size = new Size(940, 45);
            guna2Panel_top.TabIndex = 4;
            // 
            // guna2ControlBox3
            // 
            guna2ControlBox3.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            guna2ControlBox3.ControlBoxStyle = Guna.UI2.WinForms.Enums.ControlBoxStyle.Custom;
            guna2ControlBox3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            guna2ControlBox3.CustomizableEdges = customizableEdges11;
            guna2ControlBox3.FillColor = Color.Transparent;
            guna2ControlBox3.IconColor = Color.FromArgb(254, 205, 220);
            guna2ControlBox3.Location = new Point(775, 0);
            guna2ControlBox3.Name = "guna2ControlBox3";
            guna2ControlBox3.ShadowDecoration.CustomizableEdges = customizableEdges12;
            guna2ControlBox3.Size = new Size(55, 44);
            guna2ControlBox3.TabIndex = 2;
            // 
            // guna2ControlBox2
            // 
            guna2ControlBox2.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            guna2ControlBox2.ControlBoxStyle = Guna.UI2.WinForms.Enums.ControlBoxStyle.Custom;
            guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox;
            guna2ControlBox2.CustomizableEdges = customizableEdges13;
            guna2ControlBox2.FillColor = Color.Transparent;
            guna2ControlBox2.IconColor = Color.FromArgb(254, 205, 220);
            guna2ControlBox2.Location = new Point(830, 0);
            guna2ControlBox2.Name = "guna2ControlBox2";
            guna2ControlBox2.ShadowDecoration.CustomizableEdges = customizableEdges14;
            guna2ControlBox2.Size = new Size(55, 44);
            guna2ControlBox2.TabIndex = 2;
            // 
            // guna2ControlBox1
            // 
            guna2ControlBox1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            guna2ControlBox1.ControlBoxStyle = Guna.UI2.WinForms.Enums.ControlBoxStyle.Custom;
            guna2ControlBox1.CustomizableEdges = customizableEdges15;
            guna2ControlBox1.FillColor = Color.Transparent;
            guna2ControlBox1.IconColor = Color.FromArgb(254, 205, 220);
            guna2ControlBox1.Location = new Point(885, 0);
            guna2ControlBox1.Name = "guna2ControlBox1";
            guna2ControlBox1.ShadowDecoration.CustomizableEdges = customizableEdges16;
            guna2ControlBox1.Size = new Size(55, 44);
            guna2ControlBox1.TabIndex = 1;
            guna2ControlBox1.Click += guna2ControlBox1_Click;
            // 
            // label_val
            // 
            label_val.AutoSize = true;
            label_val.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label_val.ForeColor = Color.FromArgb(254, 205, 220);
            label_val.Location = new Point(6, 9);
            label_val.Name = "label_val";
            label_val.Size = new Size(42, 25);
            label_val.TabIndex = 0;
            label_val.Text = "VAL";
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(254, 205, 220);
            guna2Panel1.Controls.Add(btnNhanVien);
            guna2Panel1.Controls.Add(btnBanHang);
            guna2Panel1.Controls.Add(btnDangXuat);
            guna2Panel1.Controls.Add(btnKhachHang);
            guna2Panel1.Controls.Add(pictureBox1);
            guna2Panel1.Controls.Add(btnHoaDon);
            guna2Panel1.Controls.Add(btnHangHoa);
            guna2Panel1.Controls.Add(btnNhaCungCap);
            guna2Panel1.Controls.Add(btnNhomHangHoa);
            guna2Panel1.Controls.Add(btnOverview);
            guna2Panel1.CustomBorderColor = Color.Black;
            guna2Panel1.CustomizableEdges = customizableEdges27;
            guna2Panel1.Dock = DockStyle.Left;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges28;
            guna2Panel1.Size = new Size(250, 707);
            guna2Panel1.TabIndex = 3;
            // 
            // btnNhanVien
            // 
            btnNhanVien.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnNhanVien.CustomizableEdges = customizableEdges19;
            btnNhanVien.DisabledState.BorderColor = Color.DarkGray;
            btnNhanVien.DisabledState.CustomBorderColor = Color.DarkGray;
            btnNhanVien.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnNhanVien.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnNhanVien.FillColor = Color.Transparent;
            btnNhanVien.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnNhanVien.ForeColor = Color.Black;
            btnNhanVien.HoverState.BorderColor = Color.Navy;
            btnNhanVien.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnNhanVien.HoverState.ForeColor = Color.White;
            btnNhanVien.Image = (Image)resources.GetObject("btnNhanVien.Image");
            btnNhanVien.ImageAlign = HorizontalAlignment.Left;
            btnNhanVien.ImageOffset = new Point(8, 0);
            btnNhanVien.ImageSize = new Size(30, 30);
            btnNhanVien.ImeMode = ImeMode.On;
            btnNhanVien.Location = new Point(1, 582);
            btnNhanVien.Name = "btnNhanVien";
            btnNhanVien.ShadowDecoration.CustomizableEdges = customizableEdges20;
            btnNhanVien.Size = new Size(248, 60);
            btnNhanVien.TabIndex = 10;
            btnNhanVien.Text = "Nhân Viên";
            btnNhanVien.Click += btnNhanVien_Click;
            // 
            // btnBanHang
            // 
            btnBanHang.CustomizableEdges = customizableEdges21;
            btnBanHang.DisabledState.BorderColor = Color.DarkGray;
            btnBanHang.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBanHang.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBanHang.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBanHang.FillColor = Color.Transparent;
            btnBanHang.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnBanHang.ForeColor = Color.Black;
            btnBanHang.HoverState.BorderColor = Color.Navy;
            btnBanHang.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnBanHang.HoverState.ForeColor = Color.White;
            btnBanHang.Image = (Image)resources.GetObject("btnBanHang.Image");
            btnBanHang.ImageAlign = HorizontalAlignment.Left;
            btnBanHang.ImageOffset = new Point(8, 0);
            btnBanHang.ImageSize = new Size(30, 30);
            btnBanHang.ImeMode = ImeMode.On;
            btnBanHang.Location = new Point(1, 517);
            btnBanHang.Name = "btnBanHang";
            btnBanHang.ShadowDecoration.CustomizableEdges = customizableEdges22;
            btnBanHang.Size = new Size(248, 60);
            btnBanHang.TabIndex = 9;
            btnBanHang.Text = "Bán hàng";
            btnBanHang.Click += btnBanHang_Click;
            // 
            // btnDangXuat
            // 
            btnDangXuat.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnDangXuat.CustomizableEdges = customizableEdges23;
            btnDangXuat.DisabledState.BorderColor = Color.DarkGray;
            btnDangXuat.DisabledState.CustomBorderColor = Color.DarkGray;
            btnDangXuat.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnDangXuat.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnDangXuat.FillColor = Color.Transparent;
            btnDangXuat.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnDangXuat.ForeColor = Color.Black;
            btnDangXuat.HoverState.BorderColor = Color.Navy;
            btnDangXuat.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnDangXuat.HoverState.ForeColor = Color.White;
            btnDangXuat.Image = (Image)resources.GetObject("btnDangXuat.Image");
            btnDangXuat.ImageAlign = HorizontalAlignment.Left;
            btnDangXuat.ImageOffset = new Point(8, 0);
            btnDangXuat.ImageSize = new Size(30, 30);
            btnDangXuat.ImeMode = ImeMode.On;
            btnDangXuat.Location = new Point(1, 647);
            btnDangXuat.Name = "btnDangXuat";
            btnDangXuat.ShadowDecoration.CustomizableEdges = customizableEdges24;
            btnDangXuat.Size = new Size(248, 60);
            btnDangXuat.TabIndex = 8;
            btnDangXuat.Text = "Đăng xuất";
            btnDangXuat.Click += btnDangXuat_Click;
            // 
            // btnKhachHang
            // 
            btnKhachHang.CustomizableEdges = customizableEdges25;
            btnKhachHang.DisabledState.BorderColor = Color.DarkGray;
            btnKhachHang.DisabledState.CustomBorderColor = Color.DarkGray;
            btnKhachHang.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnKhachHang.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnKhachHang.FillColor = Color.Transparent;
            btnKhachHang.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            btnKhachHang.ForeColor = Color.Black;
            btnKhachHang.HoverState.BorderColor = Color.Navy;
            btnKhachHang.HoverState.FillColor = Color.FromArgb(50, 55, 89);
            btnKhachHang.HoverState.ForeColor = Color.White;
            btnKhachHang.Image = (Image)resources.GetObject("btnKhachHang.Image");
            btnKhachHang.ImageAlign = HorizontalAlignment.Left;
            btnKhachHang.ImageOffset = new Point(8, 0);
            btnKhachHang.ImageSize = new Size(30, 30);
            btnKhachHang.ImeMode = ImeMode.On;
            btnKhachHang.Location = new Point(1, 452);
            btnKhachHang.Name = "btnKhachHang";
            btnKhachHang.ShadowDecoration.CustomizableEdges = customizableEdges26;
            btnKhachHang.Size = new Size(248, 60);
            btnKhachHang.TabIndex = 7;
            btnKhachHang.Text = "Khách hàng";
            btnKhachHang.Click += btnKhachHang_Click;
            // 
            // guna2Panel2
            // 
            guna2Panel2.CustomizableEdges = customizableEdges29;
            guna2Panel2.Dock = DockStyle.Fill;
            guna2Panel2.Location = new Point(250, 45);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges30;
            guna2Panel2.Size = new Size(940, 662);
            guna2Panel2.TabIndex = 5;
            // 
            // FrmMain
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.White;
            ClientSize = new Size(1190, 707);
            Controls.Add(guna2Panel2);
            Controls.Add(guna2Panel_top);
            Controls.Add(guna2Panel1);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmMain";
            StartPosition = FormStartPosition.CenterScreen;
            WindowState = FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            guna2Panel_top.ResumeLayout(false);
            guna2Panel_top.PerformLayout();
            guna2Panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Button btnOverview;
        private Guna.UI2.WinForms.Guna2Button btnNhomHangHoa;
        private Guna.UI2.WinForms.Guna2Button btnNhaCungCap;
        private Guna.UI2.WinForms.Guna2Button btnHoaDon;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private Guna.UI2.WinForms.Guna2Button btnHangHoa;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel_top;
        private Label label_val;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox3;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2Button btnKhachHang;
        private Guna.UI2.WinForms.Guna2Button btnDangXuat;
        private Guna.UI2.WinForms.Guna2Button btnBanHang;
        private Guna.UI2.WinForms.Guna2Button btnNhanVien;
    }
}