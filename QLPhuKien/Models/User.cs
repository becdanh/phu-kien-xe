﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    [Table("User")]
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [StringLength(50)]
        public string Username { get; set; }
        public string Password { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }
        public bool IsAdmin { get; set; }
    }
}
