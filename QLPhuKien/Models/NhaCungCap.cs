﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QLPhuKien.Models
{
    [Table("NhaCungCap")]
    public class NhaCungCap
    {
        [Key]
        [StringLength(10)] 
        public string MaNCC { get; set; }

        [StringLength(100)] 
        public string TenNCC { get; set; }

        [StringLength(100)] 
        public string Email { get; set; }

        [StringLength(10)] 
        public string DienThoai { get; set; }

        [StringLength(200)] 
        public string DiaChi { get; set; }

        public DateTime NgayTao { get; set; }
        public bool Disabled { get; set; }
    }

}
