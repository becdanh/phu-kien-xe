﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    public class PhuKienDB : DbContext
    {
        public DbSet<NhaCungCap> nhaCungCaps { get; set; }
        public DbSet<NhomHangHoa> nhomHangHoas { get; set; }
        public DbSet<HangHoa> hangHoas { get; set; }
        public DbSet<User> users { get; set; }
        public DbSet<HoaDon> hoaDons { get; set; }
        public DbSet<ChiTietHoaDon> chiTietHoaDons { get; set; }
        public DbSet<KhachHang> khachHangs { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var path = Path.Combine("D:\\DB dotnet tutorial", "PhuKien.db");
            var connectionString = $"Data source={path}";
            optionsBuilder.UseSqlite(connectionString);
        }

    }
}
