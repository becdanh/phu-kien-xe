﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    [Table("NhomHangHoa")]
    public class NhomHangHoa
    {
        [Key]
        [StringLength(20)]
        public string MaNHH { get; set; }
        public string TenNHH { get; set; }
        [StringLength(100)]
        public string? GhiChu { get; set; }
        public virtual List<HangHoa> HangHoas { get; set;}
    }
}
