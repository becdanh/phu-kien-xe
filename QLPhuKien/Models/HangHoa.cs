﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    [Table("HangHoa")]
    public class HangHoa
    {
        [Key]

        [StringLength(13)]
        public string Barcode { get; set; }

        [StringLength(100)]
        public string TenHangHoa { get; set; }

        [StringLength(20)]
        public string DonVi { get; set; }
        public decimal GiaBan { get; set; }
        public int SoLuong { get; set; }
        public string MaNHH { get; set; }
        public string MaNCC { get; set; }

        [StringLength(255)]
        public string Anh { get; set; }
        public bool Disabled { get; set; }

        [ForeignKey("MaNHH")]
        public virtual NhomHangHoa NhomHangHoa { get; set; }

        [ForeignKey("MaNCC")]
        public virtual NhaCungCap NhaCungCap { get; set; }
    }
}
