﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    [Table("KhachHang")]
    public class KhachHang
    {
        [Key]
        public int CustomerId { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }

        [StringLength(11)]
        public string SDT { get; set; }

        [StringLength(100)]
        public string DiaChi { get; set; }
    }
}
