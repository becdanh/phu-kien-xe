﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    [Table("ChiTietHoaDon")]
    public class ChiTietHoaDon
    {
        [Key]
        public int ChiTietHoaDonId { get; set; }

        public int HoaDonId { get; set; }

        [ForeignKey("HoaDonId")]
        public virtual HoaDon HoaDon { get; set; }

        [StringLength(13)]
        public string Barcode { get; set; }

        [ForeignKey("Barcode")]
        public virtual HangHoa HangHoa { get; set; }
        public int SoLuongMua { get; set; }

        public decimal GiaBan { get; set; }

        public decimal ThanhTien { get; set; }

    }
}
