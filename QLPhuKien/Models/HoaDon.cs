﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.Models
{
    [Table("HoaDon")]
    public class HoaDon
    {
        [Key]
        public int HoaDonId { get; set; }
        public DateTime NgayTao { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public decimal TongTien { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("CustomerId")]
        public virtual KhachHang KhachHang { get; set; }
        public virtual List<ChiTietHoaDon> ChiTietHoaDons { get; set; }
    }
}
