﻿namespace QLPhuKien
{
    partial class FrmBanHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges6 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges7 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBanHang));
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges8 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges9 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges10 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges11 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges12 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges13 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges14 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges15 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            btnInHD = new Guna.UI2.WinForms.Guna2TileButton();
            btnThoat = new Guna.UI2.WinForms.Guna2ImageButton();
            btnBan = new Guna.UI2.WinForms.Guna2TileButton();
            lblTieuDe = new Label();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            lblTotalAmount = new Label();
            panelDanhMuc = new FlowLayoutPanel();
            PanelProduct = new FlowLayoutPanel();
            dtgChiTietHoaDon = new Guna.UI2.WinForms.Guna2DataGridView();
            tenHHDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            soLuongMuaDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            giaBanDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            thanhTienDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            chiTietHoaDonDTOBindingSource = new BindingSource(components);
            guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(components);
            txtTimKiem = new Guna.UI2.WinForms.Guna2TextBox();
            cbbKhachHang = new Guna.UI2.WinForms.Guna2ComboBox();
            txtSDT = new Guna.UI2.WinForms.Guna2TextBox();
            guna2Panel1.SuspendLayout();
            guna2Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dtgChiTietHoaDon).BeginInit();
            ((System.ComponentModel.ISupportInitialize)chiTietHoaDonDTOBindingSource).BeginInit();
            SuspendLayout();
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(btnInHD);
            guna2Panel1.Controls.Add(btnThoat);
            guna2Panel1.Controls.Add(btnBan);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges6;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges7;
            guna2Panel1.Size = new Size(1124, 118);
            guna2Panel1.TabIndex = 54;
            // 
            // btnInHD
            // 
            btnInHD.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            btnInHD.BorderRadius = 10;
            btnInHD.CustomizableEdges = customizableEdges1;
            btnInHD.DisabledState.BorderColor = Color.DarkGray;
            btnInHD.DisabledState.CustomBorderColor = Color.DarkGray;
            btnInHD.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnInHD.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnInHD.FillColor = Color.FromArgb(254, 205, 220);
            btnInHD.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnInHD.ForeColor = Color.Black;
            btnInHD.Image = (Image)resources.GetObject("btnInHD.Image");
            btnInHD.ImageSize = new Size(30, 30);
            btnInHD.Location = new Point(942, 18);
            btnInHD.Name = "btnInHD";
            btnInHD.ShadowDecoration.CustomizableEdges = customizableEdges2;
            btnInHD.Size = new Size(84, 82);
            btnInHD.TabIndex = 13;
            btnInHD.Text = "In HĐ";
            btnInHD.Click += btnInHD_Click;
            // 
            // btnThoat
            // 
            btnThoat.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            btnThoat.CheckedState.ImageSize = new Size(64, 64);
            btnThoat.HoverState.ImageSize = new Size(64, 64);
            btnThoat.Image = (Image)resources.GetObject("btnThoat.Image");
            btnThoat.ImageOffset = new Point(0, 0);
            btnThoat.ImageRotate = 0F;
            btnThoat.ImageSize = new Size(40, 40);
            btnThoat.Location = new Point(1041, 36);
            btnThoat.Name = "btnThoat";
            btnThoat.PressedState.ImageSize = new Size(64, 64);
            btnThoat.ShadowDecoration.CustomizableEdges = customizableEdges3;
            btnThoat.Size = new Size(47, 45);
            btnThoat.TabIndex = 12;
            btnThoat.Click += btnThoat_Click;
            // 
            // btnBan
            // 
            btnBan.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            btnBan.BorderRadius = 10;
            btnBan.CustomizableEdges = customizableEdges4;
            btnBan.DisabledState.BorderColor = Color.DarkGray;
            btnBan.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBan.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBan.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBan.FillColor = Color.FromArgb(254, 205, 220);
            btnBan.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnBan.ForeColor = Color.Black;
            btnBan.Image = Properties.Resources.sale;
            btnBan.ImageSize = new Size(30, 30);
            btnBan.Location = new Point(839, 18);
            btnBan.Name = "btnBan";
            btnBan.ShadowDecoration.CustomizableEdges = customizableEdges5;
            btnBan.Size = new Size(84, 82);
            btnBan.TabIndex = 7;
            btnBan.Text = "Bán";
            btnBan.Click += btnBan_Click;
            // 
            // lblTieuDe
            // 
            lblTieuDe.Anchor = AnchorStyles.Top;
            lblTieuDe.AutoSize = true;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(438, 36);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(167, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "BÁN HÀNG";
            // 
            // guna2Panel2
            // 
            guna2Panel2.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel2.Controls.Add(lblTotalAmount);
            guna2Panel2.CustomizableEdges = customizableEdges8;
            guna2Panel2.Dock = DockStyle.Bottom;
            guna2Panel2.Location = new Point(0, 520);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges9;
            guna2Panel2.Size = new Size(1124, 76);
            guna2Panel2.TabIndex = 55;
            // 
            // lblTotalAmount
            // 
            lblTotalAmount.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            lblTotalAmount.AutoSize = true;
            lblTotalAmount.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTotalAmount.ForeColor = Color.FromArgb(254, 205, 220);
            lblTotalAmount.Location = new Point(756, 16);
            lblTotalAmount.Name = "lblTotalAmount";
            lblTotalAmount.Size = new Size(152, 41);
            lblTotalAmount.TabIndex = 1;
            lblTotalAmount.Text = "Tổng tiền:";
            // 
            // panelDanhMuc
            // 
            panelDanhMuc.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            panelDanhMuc.Location = new Point(0, 187);
            panelDanhMuc.Name = "panelDanhMuc";
            panelDanhMuc.Size = new Size(172, 333);
            panelDanhMuc.TabIndex = 56;
            // 
            // PanelProduct
            // 
            PanelProduct.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            PanelProduct.Location = new Point(178, 179);
            PanelProduct.Name = "PanelProduct";
            PanelProduct.Size = new Size(494, 341);
            PanelProduct.TabIndex = 57;
            // 
            // dtgChiTietHoaDon
            // 
            dtgChiTietHoaDon.AllowUserToAddRows = false;
            dtgChiTietHoaDon.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = Color.White;
            dtgChiTietHoaDon.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dtgChiTietHoaDon.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            dtgChiTietHoaDon.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = Color.FromArgb(232, 234, 237);
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = Color.FromArgb(232, 234, 237);
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            dtgChiTietHoaDon.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dtgChiTietHoaDon.ColumnHeadersHeight = 40;
            dtgChiTietHoaDon.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dtgChiTietHoaDon.Columns.AddRange(new DataGridViewColumn[] { tenHHDataGridViewTextBoxColumn, soLuongMuaDataGridViewTextBoxColumn, giaBanDataGridViewTextBoxColumn, thanhTienDataGridViewTextBoxColumn });
            dtgChiTietHoaDon.DataSource = chiTietHoaDonDTOBindingSource;
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = Color.White;
            dataGridViewCellStyle3.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = Color.FromArgb(71, 69, 94);
            dataGridViewCellStyle3.SelectionBackColor = Color.FromArgb(239, 241, 243);
            dataGridViewCellStyle3.SelectionForeColor = Color.FromArgb(71, 69, 94);
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
            dtgChiTietHoaDon.DefaultCellStyle = dataGridViewCellStyle3;
            dtgChiTietHoaDon.GridColor = Color.FromArgb(231, 229, 255);
            dtgChiTietHoaDon.Location = new Point(678, 187);
            dtgChiTietHoaDon.Name = "dtgChiTietHoaDon";
            dtgChiTietHoaDon.ReadOnly = true;
            dtgChiTietHoaDon.RowHeadersVisible = false;
            dtgChiTietHoaDon.RowHeadersWidth = 51;
            dtgChiTietHoaDon.RowTemplate.Height = 29;
            dtgChiTietHoaDon.Size = new Size(434, 322);
            dtgChiTietHoaDon.TabIndex = 58;
            dtgChiTietHoaDon.ThemeStyle.AlternatingRowsStyle.BackColor = Color.White;
            dtgChiTietHoaDon.ThemeStyle.AlternatingRowsStyle.Font = null;
            dtgChiTietHoaDon.ThemeStyle.AlternatingRowsStyle.ForeColor = Color.Empty;
            dtgChiTietHoaDon.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = Color.Empty;
            dtgChiTietHoaDon.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = Color.Empty;
            dtgChiTietHoaDon.ThemeStyle.BackColor = Color.White;
            dtgChiTietHoaDon.ThemeStyle.GridColor = Color.FromArgb(231, 229, 255);
            dtgChiTietHoaDon.ThemeStyle.HeaderStyle.BackColor = Color.FromArgb(100, 88, 255);
            dtgChiTietHoaDon.ThemeStyle.HeaderStyle.BorderStyle = DataGridViewHeaderBorderStyle.None;
            dtgChiTietHoaDon.ThemeStyle.HeaderStyle.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dtgChiTietHoaDon.ThemeStyle.HeaderStyle.ForeColor = Color.White;
            dtgChiTietHoaDon.ThemeStyle.HeaderStyle.HeaightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dtgChiTietHoaDon.ThemeStyle.HeaderStyle.Height = 40;
            dtgChiTietHoaDon.ThemeStyle.ReadOnly = true;
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.BackColor = Color.White;
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.BorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.ForeColor = Color.FromArgb(71, 69, 94);
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.Height = 29;
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.SelectionBackColor = Color.FromArgb(231, 229, 255);
            dtgChiTietHoaDon.ThemeStyle.RowsStyle.SelectionForeColor = Color.FromArgb(71, 69, 94);
            dtgChiTietHoaDon.CellDoubleClick += dtgChiTietHoaDon_CellDoubleClick;
            // 
            // tenHHDataGridViewTextBoxColumn
            // 
            tenHHDataGridViewTextBoxColumn.DataPropertyName = "TenHH";
            tenHHDataGridViewTextBoxColumn.FillWeight = 36.87266F;
            tenHHDataGridViewTextBoxColumn.HeaderText = "Hàng hóa";
            tenHHDataGridViewTextBoxColumn.MinimumWidth = 6;
            tenHHDataGridViewTextBoxColumn.Name = "tenHHDataGridViewTextBoxColumn";
            tenHHDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // soLuongMuaDataGridViewTextBoxColumn
            // 
            soLuongMuaDataGridViewTextBoxColumn.DataPropertyName = "SoLuongMua";
            soLuongMuaDataGridViewTextBoxColumn.FillWeight = 20.5433388F;
            soLuongMuaDataGridViewTextBoxColumn.HeaderText = "Số lượng";
            soLuongMuaDataGridViewTextBoxColumn.MinimumWidth = 6;
            soLuongMuaDataGridViewTextBoxColumn.Name = "soLuongMuaDataGridViewTextBoxColumn";
            soLuongMuaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // giaBanDataGridViewTextBoxColumn
            // 
            giaBanDataGridViewTextBoxColumn.DataPropertyName = "GiaBan";
            giaBanDataGridViewTextBoxColumn.FillWeight = 20.5433388F;
            giaBanDataGridViewTextBoxColumn.HeaderText = "Giá";
            giaBanDataGridViewTextBoxColumn.MinimumWidth = 6;
            giaBanDataGridViewTextBoxColumn.Name = "giaBanDataGridViewTextBoxColumn";
            giaBanDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // thanhTienDataGridViewTextBoxColumn
            // 
            thanhTienDataGridViewTextBoxColumn.DataPropertyName = "ThanhTien";
            thanhTienDataGridViewTextBoxColumn.FillWeight = 20.5433388F;
            thanhTienDataGridViewTextBoxColumn.HeaderText = "Thành tiền";
            thanhTienDataGridViewTextBoxColumn.MinimumWidth = 6;
            thanhTienDataGridViewTextBoxColumn.Name = "thanhTienDataGridViewTextBoxColumn";
            thanhTienDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // chiTietHoaDonDTOBindingSource
            // 
            chiTietHoaDonDTOBindingSource.DataSource = typeof(DTO.ChiTietHoaDonDTO);
            // 
            // guna2DragControl1
            // 
            guna2DragControl1.DockIndicatorTransparencyValue = 0.6D;
            guna2DragControl1.UseTransparentDrag = true;
            // 
            // txtTimKiem
            // 
            txtTimKiem.AutoRoundedCorners = true;
            txtTimKiem.BorderRadius = 21;
            txtTimKiem.CustomizableEdges = customizableEdges10;
            txtTimKiem.DefaultText = "";
            txtTimKiem.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtTimKiem.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtTimKiem.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtTimKiem.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtTimKiem.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTimKiem.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtTimKiem.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTimKiem.IconLeft = (Image)resources.GetObject("txtTimKiem.IconLeft");
            txtTimKiem.IconLeftOffset = new Point(5, 0);
            txtTimKiem.Location = new Point(18, 132);
            txtTimKiem.Name = "txtTimKiem";
            txtTimKiem.PasswordChar = '\0';
            txtTimKiem.PlaceholderText = "Search in the bóc";
            txtTimKiem.SelectedText = "";
            txtTimKiem.ShadowDecoration.CustomizableEdges = customizableEdges11;
            txtTimKiem.Size = new Size(319, 45);
            txtTimKiem.TabIndex = 59;
            txtTimKiem.KeyPress += txtTimKiem_KeyPress;
            // 
            // cbbKhachHang
            // 
            cbbKhachHang.BackColor = Color.Transparent;
            cbbKhachHang.CustomizableEdges = customizableEdges12;
            cbbKhachHang.DrawMode = DrawMode.OwnerDrawFixed;
            cbbKhachHang.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbKhachHang.FocusedColor = Color.FromArgb(94, 148, 255);
            cbbKhachHang.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            cbbKhachHang.Font = new Font("Segoe UI", 10F, FontStyle.Regular, GraphicsUnit.Point);
            cbbKhachHang.ForeColor = Color.FromArgb(68, 88, 112);
            cbbKhachHang.ItemHeight = 37;
            cbbKhachHang.Location = new Point(348, 132);
            cbbKhachHang.Name = "cbbKhachHang";
            cbbKhachHang.ShadowDecoration.CustomizableEdges = customizableEdges13;
            cbbKhachHang.Size = new Size(325, 43);
            cbbKhachHang.TabIndex = 60;
            cbbKhachHang.SelectedIndexChanged += cbbKhachHang_SelectedIndexChanged;
            // 
            // txtSDT
            // 
            txtSDT.CustomizableEdges = customizableEdges14;
            txtSDT.DefaultText = "";
            txtSDT.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtSDT.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtSDT.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtSDT.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtSDT.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtSDT.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtSDT.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtSDT.Location = new Point(686, 132);
            txtSDT.Name = "txtSDT";
            txtSDT.PasswordChar = '\0';
            txtSDT.PlaceholderText = "";
            txtSDT.SelectedText = "";
            txtSDT.ShadowDecoration.CustomizableEdges = customizableEdges15;
            txtSDT.Size = new Size(195, 43);
            txtSDT.TabIndex = 61;
            // 
            // FrmBanHang
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1124, 596);
            Controls.Add(txtSDT);
            Controls.Add(cbbKhachHang);
            Controls.Add(txtTimKiem);
            Controls.Add(dtgChiTietHoaDon);
            Controls.Add(PanelProduct);
            Controls.Add(panelDanhMuc);
            Controls.Add(guna2Panel1);
            Controls.Add(guna2Panel2);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmBanHang";
            Text = "FrmBanHang";
            WindowState = FormWindowState.Maximized;
            guna2Panel1.ResumeLayout(false);
            guna2Panel1.PerformLayout();
            guna2Panel2.ResumeLayout(false);
            guna2Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dtgChiTietHoaDon).EndInit();
            ((System.ComponentModel.ISupportInitialize)chiTietHoaDonDTOBindingSource).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Label lblTieuDe;
        private FlowLayoutPanel panelDanhMuc;
        private FlowLayoutPanel PanelProduct;
        private Guna.UI2.WinForms.Guna2DataGridView dtgChiTietHoaDon;
        private BindingSource chiTietHoaDonDTOBindingSource;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private DataGridViewTextBoxColumn tenHHDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn soLuongMuaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn giaBanDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn thanhTienDataGridViewTextBoxColumn;
        private Guna.UI2.WinForms.Guna2TextBox txtTimKiem;
        private Guna.UI2.WinForms.Guna2ComboBox cbbKhachHang;
        private Guna.UI2.WinForms.Guna2TileButton btnBan;
        private Guna.UI2.WinForms.Guna2ImageButton btnThoat;
        private Guna.UI2.WinForms.Guna2TileButton btnInHD;
        private Label lblTotalAmount;
        private Guna.UI2.WinForms.Guna2TextBox txtSDT;
    }
}