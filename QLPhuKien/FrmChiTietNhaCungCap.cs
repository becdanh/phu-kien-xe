﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmChiTietNhaCungCap : Form
    {
        NhaCungCapDTO nhacungcap;
        public FrmChiTietNhaCungCap(NhaCungCapDTO nhacungcap = null)
        {
            InitializeComponent();
            this.nhacungcap = nhacungcap;
            if (nhacungcap != null)
            {
                //Cap nhat
                lblTieuDe.Text = "CẬP NHẬT NHÀ CUNG CẤP";
                txtMaNCC.Enabled = false;
                txtMaNCC.Text = nhacungcap.MaNCC;
                txtTenNCC.Text = nhacungcap.TenNCC;
                txtSDT.Text = nhacungcap.DienThoai;
                txtEmail.Text = nhacungcap.Email;
                txtDiaChi.Text = nhacungcap.DiaChi;
                checkboxNCC.Checked = nhacungcap.Disabled;
            }
        }


        private void btnBoQua_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool IsPhoneNumberValid(string phoneNumber)
        {
            string phonePattern = @"^\d{10}$"; // Assumes a 10-digit phone number
            return Regex.IsMatch(phoneNumber, phonePattern);
        }

        private bool IsEmailValid(string email)
        {
            string emailPattern = @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
            return Regex.IsMatch(email, emailPattern);
        }
        private void btnDongY_Click(object sender, EventArgs e)
        {
            using (var db = new PhuKienDB())
            {
                if (nhacungcap == null)
                {
                    if (string.IsNullOrWhiteSpace(txtMaNCC.Text)
                || string.IsNullOrWhiteSpace(txtTenNCC.Text)
                || string.IsNullOrWhiteSpace(txtEmail.Text)
                || string.IsNullOrWhiteSpace(txtSDT.Text)
                || string.IsNullOrWhiteSpace(txtDiaChi.Text))
                    {
                        MessageBox.Show("Vui lòng nhập đầy đủ thông tin.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    // Check for duplicate supplier code
                    if (db.nhaCungCaps.Any(t => t.MaNCC == txtMaNCC.Text))
                    {
                        MessageBox.Show("Mã nhà cung cấp đã tồn tại. Vui lòng chọn mã khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Check for duplicate email
                    if (db.nhaCungCaps.Any(t => t.Email == txtEmail.Text))
                    {
                        MessageBox.Show("Email đã tồn tại. Vui lòng chọn email khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Check for duplicate phone number
                    if (db.nhaCungCaps.Any(t => t.DienThoai == txtSDT.Text))
                    {
                        MessageBox.Show("Số điện thoại đã tồn tại. Vui lòng chọn số điện thoại khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (!IsPhoneNumberValid(txtSDT.Text))
                    {
                        MessageBox.Show("Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsEmailValid(txtEmail.Text))
                    {
                        MessageBox.Show("Email không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var ncc = new Models.NhaCungCap
                    {
                        MaNCC = txtMaNCC.Text,
                        TenNCC = txtTenNCC.Text,
                        Email = txtEmail.Text,
                        DienThoai = txtSDT.Text,
                        DiaChi = txtDiaChi.Text,
                        NgayTao = DateTime.Now,
                        Disabled = checkboxNCC.Checked
                    };
                    db.nhaCungCaps.Add(ncc);
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;

                }
                else
                {
                    if (db.nhaCungCaps.Any(t => t.Email == txtEmail.Text && t.MaNCC != nhacungcap.MaNCC))
                    {
                        MessageBox.Show("Email đã tồn tại. Vui lòng chọn email khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Check for duplicate phone number (excluding the current record being edited)
                    if (db.nhaCungCaps.Any(t => t.DienThoai == txtSDT.Text && t.MaNCC != nhacungcap.MaNCC))
                    {
                        MessageBox.Show("Số điện thoại đã tồn tại. Vui lòng chọn số điện thoại khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsPhoneNumberValid(txtSDT.Text))
                    {
                        MessageBox.Show("Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsEmailValid(txtEmail.Text))
                    {
                        MessageBox.Show("Email không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var ncc = db.nhaCungCaps.Where(t => t.MaNCC == nhacungcap.MaNCC).FirstOrDefault();
                    ncc.TenNCC = txtTenNCC.Text;
                    ncc.Email = txtEmail.Text;
                    ncc.DienThoai = txtSDT.Text;
                    ncc.DiaChi = txtDiaChi.Text;
                    ncc.Disabled = checkboxNCC.Checked;
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
