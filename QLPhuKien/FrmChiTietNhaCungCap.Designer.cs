﻿namespace QLPhuKien
{
    partial class FrmChiTietNhaCungCap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges7 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges8 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges6 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges9 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges10 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges11 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges12 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges13 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges14 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges15 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges16 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges17 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges18 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            lblTieuDe = new Label();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            btnBoQua = new Guna.UI2.WinForms.Guna2Button();
            btnDongY = new Guna.UI2.WinForms.Guna2Button();
            txtDiaChi = new Guna.UI2.WinForms.Guna2TextBox();
            label4 = new Label();
            txtTenNCC = new Guna.UI2.WinForms.Guna2TextBox();
            label3 = new Label();
            txtMaNCC = new Guna.UI2.WinForms.Guna2TextBox();
            label2 = new Label();
            txtEmail = new Guna.UI2.WinForms.Guna2TextBox();
            label1 = new Label();
            txtSDT = new Guna.UI2.WinForms.Guna2TextBox();
            label5 = new Label();
            checkboxNCC = new Guna.UI2.WinForms.Guna2CheckBox();
            guna2Panel1.SuspendLayout();
            guna2Panel2.SuspendLayout();
            SuspendLayout();
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges1;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges2;
            guna2Panel1.Size = new Size(997, 118);
            guna2Panel1.TabIndex = 8;
            // 
            // lblTieuDe
            // 
            lblTieuDe.AutoSize = true;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(351, 36);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(321, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "THÊM NHÀ CUNG CẤP";
            // 
            // guna2Panel2
            // 
            guna2Panel2.BackColor = Color.Gainsboro;
            guna2Panel2.Controls.Add(btnBoQua);
            guna2Panel2.Controls.Add(btnDongY);
            guna2Panel2.CustomizableEdges = customizableEdges7;
            guna2Panel2.Dock = DockStyle.Bottom;
            guna2Panel2.Location = new Point(0, 352);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges8;
            guna2Panel2.Size = new Size(997, 76);
            guna2Panel2.TabIndex = 9;
            // 
            // btnBoQua
            // 
            btnBoQua.AutoRoundedCorners = true;
            btnBoQua.BorderRadius = 21;
            customizableEdges3.TopRight = false;
            btnBoQua.CustomizableEdges = customizableEdges3;
            btnBoQua.DisabledState.BorderColor = Color.DarkGray;
            btnBoQua.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBoQua.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBoQua.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBoQua.FillColor = Color.FromArgb(50, 55, 89);
            btnBoQua.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnBoQua.ForeColor = Color.White;
            btnBoQua.Location = new Point(873, 16);
            btnBoQua.Name = "btnBoQua";
            btnBoQua.ShadowDecoration.CustomizableEdges = customizableEdges4;
            btnBoQua.Size = new Size(94, 45);
            btnBoQua.TabIndex = 6;
            btnBoQua.Text = "Bỏ qua";
            btnBoQua.Click += btnBoQua_Click;
            // 
            // btnDongY
            // 
            btnDongY.AutoRoundedCorners = true;
            btnDongY.BorderRadius = 21;
            customizableEdges5.TopRight = false;
            btnDongY.CustomizableEdges = customizableEdges5;
            btnDongY.DisabledState.BorderColor = Color.DarkGray;
            btnDongY.DisabledState.CustomBorderColor = Color.DarkGray;
            btnDongY.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnDongY.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnDongY.FillColor = Color.FromArgb(254, 205, 220);
            btnDongY.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnDongY.ForeColor = Color.Black;
            btnDongY.Location = new Point(761, 16);
            btnDongY.Name = "btnDongY";
            btnDongY.ShadowDecoration.CustomizableEdges = customizableEdges6;
            btnDongY.Size = new Size(90, 45);
            btnDongY.TabIndex = 5;
            btnDongY.Text = "Đồng ý";
            btnDongY.Click += btnDongY_Click;
            // 
            // txtDiaChi
            // 
            txtDiaChi.CustomizableEdges = customizableEdges9;
            txtDiaChi.DefaultText = "";
            txtDiaChi.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtDiaChi.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtDiaChi.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtDiaChi.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtDiaChi.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtDiaChi.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtDiaChi.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtDiaChi.Location = new Point(138, 288);
            txtDiaChi.Name = "txtDiaChi";
            txtDiaChi.PasswordChar = '\0';
            txtDiaChi.PlaceholderText = "";
            txtDiaChi.SelectedText = "";
            txtDiaChi.ShadowDecoration.CustomizableEdges = customizableEdges10;
            txtDiaChi.Size = new Size(706, 43);
            txtDiaChi.TabIndex = 4;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(12, 298);
            label4.Name = "label4";
            label4.Size = new Size(55, 20);
            label4.TabIndex = 14;
            label4.Text = "Địa chỉ";
            // 
            // txtTenNCC
            // 
            txtTenNCC.CustomizableEdges = customizableEdges11;
            txtTenNCC.DefaultText = "";
            txtTenNCC.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtTenNCC.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtTenNCC.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtTenNCC.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtTenNCC.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenNCC.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtTenNCC.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenNCC.Location = new Point(138, 212);
            txtTenNCC.Name = "txtTenNCC";
            txtTenNCC.PasswordChar = '\0';
            txtTenNCC.PlaceholderText = "";
            txtTenNCC.SelectedText = "";
            txtTenNCC.ShadowDecoration.CustomizableEdges = customizableEdges12;
            txtTenNCC.Size = new Size(326, 43);
            txtTenNCC.TabIndex = 1;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(12, 222);
            label3.Name = "label3";
            label3.Size = new Size(124, 20);
            label3.TabIndex = 12;
            label3.Text = "Tên nhà cung cấp";
            // 
            // txtMaNCC
            // 
            txtMaNCC.CustomizableEdges = customizableEdges13;
            txtMaNCC.DefaultText = "";
            txtMaNCC.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtMaNCC.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtMaNCC.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtMaNCC.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtMaNCC.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtMaNCC.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtMaNCC.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtMaNCC.Location = new Point(138, 137);
            txtMaNCC.Name = "txtMaNCC";
            txtMaNCC.PasswordChar = '\0';
            txtMaNCC.PlaceholderText = "";
            txtMaNCC.SelectedText = "";
            txtMaNCC.ShadowDecoration.CustomizableEdges = customizableEdges14;
            txtMaNCC.Size = new Size(326, 43);
            txtMaNCC.TabIndex = 0;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 147);
            label2.Name = "label2";
            label2.Size = new Size(122, 20);
            label2.TabIndex = 10;
            label2.Text = "Mã nhà cung cấp";
            // 
            // txtEmail
            // 
            txtEmail.CustomizableEdges = customizableEdges15;
            txtEmail.DefaultText = "";
            txtEmail.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtEmail.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtEmail.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtEmail.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtEmail.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtEmail.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtEmail.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtEmail.Location = new Point(641, 212);
            txtEmail.Name = "txtEmail";
            txtEmail.PasswordChar = '\0';
            txtEmail.PlaceholderText = "";
            txtEmail.SelectedText = "";
            txtEmail.ShadowDecoration.CustomizableEdges = customizableEdges16;
            txtEmail.Size = new Size(326, 43);
            txtEmail.TabIndex = 3;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(515, 222);
            label1.Name = "label1";
            label1.Size = new Size(46, 20);
            label1.TabIndex = 16;
            label1.Text = "Email";
            // 
            // txtSDT
            // 
            txtSDT.CustomizableEdges = customizableEdges17;
            txtSDT.DefaultText = "";
            txtSDT.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtSDT.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtSDT.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtSDT.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtSDT.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtSDT.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtSDT.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtSDT.Location = new Point(641, 137);
            txtSDT.Name = "txtSDT";
            txtSDT.PasswordChar = '\0';
            txtSDT.PlaceholderText = "";
            txtSDT.SelectedText = "";
            txtSDT.ShadowDecoration.CustomizableEdges = customizableEdges18;
            txtSDT.Size = new Size(326, 43);
            txtSDT.TabIndex = 2;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(515, 147);
            label5.Name = "label5";
            label5.Size = new Size(97, 20);
            label5.TabIndex = 18;
            label5.Text = "Số điện thoại";
            // 
            // checkboxNCC
            // 
            checkboxNCC.AutoSize = true;
            checkboxNCC.CheckedState.BorderColor = Color.FromArgb(94, 148, 255);
            checkboxNCC.CheckedState.BorderRadius = 0;
            checkboxNCC.CheckedState.BorderThickness = 0;
            checkboxNCC.CheckedState.FillColor = Color.FromArgb(94, 148, 255);
            checkboxNCC.Location = new Point(877, 298);
            checkboxNCC.Name = "checkboxNCC";
            checkboxNCC.Size = new Size(90, 24);
            checkboxNCC.TabIndex = 5;
            checkboxNCC.Text = "Disabled";
            checkboxNCC.UncheckedState.BorderColor = Color.FromArgb(125, 137, 149);
            checkboxNCC.UncheckedState.BorderRadius = 0;
            checkboxNCC.UncheckedState.BorderThickness = 0;
            checkboxNCC.UncheckedState.FillColor = Color.White;
            // 
            // FrmChiTietNhaCungCap
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(997, 428);
            Controls.Add(checkboxNCC);
            Controls.Add(txtSDT);
            Controls.Add(label5);
            Controls.Add(txtEmail);
            Controls.Add(label1);
            Controls.Add(guna2Panel1);
            Controls.Add(guna2Panel2);
            Controls.Add(txtDiaChi);
            Controls.Add(label4);
            Controls.Add(txtTenNCC);
            Controls.Add(label3);
            Controls.Add(txtMaNCC);
            Controls.Add(label2);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmChiTietNhaCungCap";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FrmChiTietNhaCungCap";
            guna2Panel1.ResumeLayout(false);
            guna2Panel1.PerformLayout();
            guna2Panel2.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Label lblTieuDe;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Button btnBoQua;
        private Guna.UI2.WinForms.Guna2Button btnDongY;
        private Guna.UI2.WinForms.Guna2TextBox txtDiaChi;
        private Label label4;
        private Guna.UI2.WinForms.Guna2TextBox txtTenNCC;
        private Label label3;
        private Guna.UI2.WinForms.Guna2TextBox txtMaNCC;
        private Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txtEmail;
        private Label label1;
        private Guna.UI2.WinForms.Guna2TextBox txtSDT;
        private Label label5;
        private Guna.UI2.WinForms.Guna2CheckBox checkboxNCC;
    }
}