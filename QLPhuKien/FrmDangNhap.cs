﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmDangNhap : Form
    {
        public FrmDangNhap()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public static UserDTO LoggedInUser { get; private set; }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtTaiKhoan.Text;
            string password = txtMatKhau.Text;

            // Kiểm tra tài khoản và mật khẩu không được để trống
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                MessageBox.Show("Vui lòng nhập tên đăng nhập và mật khẩu.");
                return;
            }

            using (var db = new PhuKienDB())
            {
                var user = db.users.FirstOrDefault(u => u.Username == username && u.Password == password);

                if (user != null)
                {
                    // Mở Form1 sau khi đăng nhập thành công
                    FrmMain form1 = new FrmMain();
                    form1.Show();
                    LoggedInUser = new UserDTO
                    {
                        UserId = user.UserId,
                        FullName = user.FullName,
                        IsAdmin = user.IsAdmin
                    };
                    this.Hide();
                }
                else
                {
                    // Đăng nhập thất bại
                    MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng!");
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
