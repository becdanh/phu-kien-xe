﻿namespace QLPhuKien
{
    partial class FrmChiTietHangHoa1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges3 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges4 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges5 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges6 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges9 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges10 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges7 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges8 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges11 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges12 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges13 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges14 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges15 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges16 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges17 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges18 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges19 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges20 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges21 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges22 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges23 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges24 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            btnDongY = new Guna.UI2.WinForms.Guna2Button();
            cbbNhaCungCap = new Guna.UI2.WinForms.Guna2ComboBox();
            label6 = new Label();
            guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            lblTieuDe = new Label();
            guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            btnBoQua = new Guna.UI2.WinForms.Guna2Button();
            txtDonGia = new Guna.UI2.WinForms.Guna2TextBox();
            label5 = new Label();
            txtDonViTinh = new Guna.UI2.WinForms.Guna2TextBox();
            txtSoLuong = new Guna.UI2.WinForms.Guna2TextBox();
            label4 = new Label();
            txtTenHangHoa = new Guna.UI2.WinForms.Guna2TextBox();
            label3 = new Label();
            txtBarcode = new Guna.UI2.WinForms.Guna2TextBox();
            label2 = new Label();
            label1 = new Label();
            cbbNhomHangHoa = new Guna.UI2.WinForms.Guna2ComboBox();
            label7 = new Label();
            checkboxHangHoa = new Guna.UI2.WinForms.Guna2CheckBox();
            label8 = new Label();
            picHangHoa = new Guna.UI2.WinForms.Guna2PictureBox();
            guna2Panel1.SuspendLayout();
            guna2Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)picHangHoa).BeginInit();
            SuspendLayout();
            // 
            // btnDongY
            // 
            btnDongY.AutoRoundedCorners = true;
            btnDongY.BorderRadius = 21;
            customizableEdges1.TopRight = false;
            btnDongY.CustomizableEdges = customizableEdges1;
            btnDongY.DisabledState.BorderColor = Color.DarkGray;
            btnDongY.DisabledState.CustomBorderColor = Color.DarkGray;
            btnDongY.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnDongY.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnDongY.FillColor = Color.FromArgb(254, 205, 220);
            btnDongY.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnDongY.ForeColor = Color.Black;
            btnDongY.Location = new Point(761, 16);
            btnDongY.Name = "btnDongY";
            btnDongY.ShadowDecoration.CustomizableEdges = customizableEdges2;
            btnDongY.Size = new Size(90, 45);
            btnDongY.TabIndex = 0;
            btnDongY.Text = "Đồng ý";
            btnDongY.Click += btnDongY_Click;
            // 
            // cbbNhaCungCap
            // 
            cbbNhaCungCap.BackColor = Color.Transparent;
            cbbNhaCungCap.CustomizableEdges = customizableEdges3;
            cbbNhaCungCap.DrawMode = DrawMode.OwnerDrawFixed;
            cbbNhaCungCap.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbNhaCungCap.FocusedColor = Color.FromArgb(94, 148, 255);
            cbbNhaCungCap.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            cbbNhaCungCap.Font = new Font("Segoe UI", 10F, FontStyle.Regular, GraphicsUnit.Point);
            cbbNhaCungCap.ForeColor = Color.FromArgb(68, 88, 112);
            cbbNhaCungCap.ItemHeight = 37;
            cbbNhaCungCap.Location = new Point(642, 291);
            cbbNhaCungCap.Name = "cbbNhaCungCap";
            cbbNhaCungCap.ShadowDecoration.CustomizableEdges = customizableEdges4;
            cbbNhaCungCap.Size = new Size(325, 43);
            cbbNhaCungCap.TabIndex = 6;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(515, 301);
            label6.Name = "label6";
            label6.Size = new Size(100, 20);
            label6.TabIndex = 47;
            label6.Text = "Nhà cung cấp";
            // 
            // guna2Panel1
            // 
            guna2Panel1.BackColor = Color.FromArgb(50, 55, 89);
            guna2Panel1.Controls.Add(lblTieuDe);
            guna2Panel1.CustomizableEdges = customizableEdges5;
            guna2Panel1.Dock = DockStyle.Top;
            guna2Panel1.Location = new Point(0, 0);
            guna2Panel1.Name = "guna2Panel1";
            guna2Panel1.ShadowDecoration.CustomizableEdges = customizableEdges6;
            guna2Panel1.Size = new Size(1015, 118);
            guna2Panel1.TabIndex = 1;
            // 
            // lblTieuDe
            // 
            lblTieuDe.AutoSize = true;
            lblTieuDe.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblTieuDe.ForeColor = Color.FromArgb(254, 205, 220);
            lblTieuDe.Location = new Point(351, 36);
            lblTieuDe.Name = "lblTieuDe";
            lblTieuDe.Size = new Size(258, 41);
            lblTieuDe.TabIndex = 0;
            lblTieuDe.Text = "THÊM HÀNG HÓA";
            // 
            // guna2Panel2
            // 
            guna2Panel2.BackColor = Color.Gainsboro;
            guna2Panel2.Controls.Add(btnBoQua);
            guna2Panel2.Controls.Add(btnDongY);
            guna2Panel2.CustomizableEdges = customizableEdges9;
            guna2Panel2.Dock = DockStyle.Bottom;
            guna2Panel2.Location = new Point(0, 535);
            guna2Panel2.Name = "guna2Panel2";
            guna2Panel2.ShadowDecoration.CustomizableEdges = customizableEdges10;
            guna2Panel2.Size = new Size(1015, 76);
            guna2Panel2.TabIndex = 36;
            // 
            // btnBoQua
            // 
            btnBoQua.AutoRoundedCorners = true;
            btnBoQua.BorderRadius = 21;
            customizableEdges7.TopRight = false;
            btnBoQua.CustomizableEdges = customizableEdges7;
            btnBoQua.DisabledState.BorderColor = Color.DarkGray;
            btnBoQua.DisabledState.CustomBorderColor = Color.DarkGray;
            btnBoQua.DisabledState.FillColor = Color.FromArgb(169, 169, 169);
            btnBoQua.DisabledState.ForeColor = Color.FromArgb(141, 141, 141);
            btnBoQua.FillColor = Color.FromArgb(50, 55, 89);
            btnBoQua.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnBoQua.ForeColor = Color.White;
            btnBoQua.Location = new Point(873, 16);
            btnBoQua.Name = "btnBoQua";
            btnBoQua.ShadowDecoration.CustomizableEdges = customizableEdges8;
            btnBoQua.Size = new Size(94, 45);
            btnBoQua.TabIndex = 1;
            btnBoQua.Text = "Bỏ qua";
            btnBoQua.Click += btnBoQua_Click;
            // 
            // txtDonGia
            // 
            txtDonGia.CustomizableEdges = customizableEdges11;
            txtDonGia.DefaultText = "";
            txtDonGia.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtDonGia.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtDonGia.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtDonGia.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtDonGia.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtDonGia.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtDonGia.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtDonGia.Location = new Point(641, 140);
            txtDonGia.Name = "txtDonGia";
            txtDonGia.PasswordChar = '\0';
            txtDonGia.PlaceholderText = "";
            txtDonGia.SelectedText = "";
            txtDonGia.ShadowDecoration.CustomizableEdges = customizableEdges12;
            txtDonGia.Size = new Size(326, 43);
            txtDonGia.TabIndex = 4;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(515, 150);
            label5.Name = "label5";
            label5.Size = new Size(62, 20);
            label5.TabIndex = 45;
            label5.Text = "Đơn giá";
            // 
            // txtDonViTinh
            // 
            txtDonViTinh.CustomizableEdges = customizableEdges13;
            txtDonViTinh.DefaultText = "";
            txtDonViTinh.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtDonViTinh.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtDonViTinh.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtDonViTinh.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtDonViTinh.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtDonViTinh.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtDonViTinh.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtDonViTinh.Location = new Point(641, 215);
            txtDonViTinh.Name = "txtDonViTinh";
            txtDonViTinh.PasswordChar = '\0';
            txtDonViTinh.PlaceholderText = "";
            txtDonViTinh.SelectedText = "";
            txtDonViTinh.ShadowDecoration.CustomizableEdges = customizableEdges14;
            txtDonViTinh.Size = new Size(326, 43);
            txtDonViTinh.TabIndex = 5;
            // 
            // txtSoLuong
            // 
            txtSoLuong.CustomizableEdges = customizableEdges15;
            txtSoLuong.DefaultText = "";
            txtSoLuong.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtSoLuong.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtSoLuong.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtSoLuong.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtSoLuong.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtSoLuong.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtSoLuong.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtSoLuong.Location = new Point(138, 366);
            txtSoLuong.Name = "txtSoLuong";
            txtSoLuong.PasswordChar = '\0';
            txtSoLuong.PlaceholderText = "";
            txtSoLuong.SelectedText = "";
            txtSoLuong.ShadowDecoration.CustomizableEdges = customizableEdges16;
            txtSoLuong.Size = new Size(326, 43);
            txtSoLuong.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(12, 376);
            label4.Name = "label4";
            label4.Size = new Size(69, 20);
            label4.TabIndex = 41;
            label4.Text = "Số lượng";
            // 
            // txtTenHangHoa
            // 
            txtTenHangHoa.CustomizableEdges = customizableEdges17;
            txtTenHangHoa.DefaultText = "";
            txtTenHangHoa.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtTenHangHoa.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtTenHangHoa.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtTenHangHoa.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtTenHangHoa.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenHangHoa.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtTenHangHoa.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtTenHangHoa.Location = new Point(138, 290);
            txtTenHangHoa.Name = "txtTenHangHoa";
            txtTenHangHoa.PasswordChar = '\0';
            txtTenHangHoa.PlaceholderText = "";
            txtTenHangHoa.SelectedText = "";
            txtTenHangHoa.ShadowDecoration.CustomizableEdges = customizableEdges18;
            txtTenHangHoa.Size = new Size(326, 43);
            txtTenHangHoa.TabIndex = 2;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(12, 300);
            label3.Name = "label3";
            label3.Size = new Size(98, 20);
            label3.TabIndex = 39;
            label3.Text = "Tên hàng hóa";
            // 
            // txtBarcode
            // 
            txtBarcode.CustomizableEdges = customizableEdges19;
            txtBarcode.DefaultText = "";
            txtBarcode.DisabledState.BorderColor = Color.FromArgb(208, 208, 208);
            txtBarcode.DisabledState.FillColor = Color.FromArgb(226, 226, 226);
            txtBarcode.DisabledState.ForeColor = Color.FromArgb(138, 138, 138);
            txtBarcode.DisabledState.PlaceholderForeColor = Color.FromArgb(138, 138, 138);
            txtBarcode.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            txtBarcode.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtBarcode.HoverState.BorderColor = Color.FromArgb(94, 148, 255);
            txtBarcode.Location = new Point(138, 215);
            txtBarcode.Name = "txtBarcode";
            txtBarcode.PasswordChar = '\0';
            txtBarcode.PlaceholderText = "";
            txtBarcode.SelectedText = "";
            txtBarcode.ShadowDecoration.CustomizableEdges = customizableEdges20;
            txtBarcode.Size = new Size(326, 43);
            txtBarcode.TabIndex = 1;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 225);
            label2.Name = "label2";
            label2.Size = new Size(64, 20);
            label2.TabIndex = 37;
            label2.Text = "Barcode";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(515, 225);
            label1.Name = "label1";
            label1.Size = new Size(81, 20);
            label1.TabIndex = 43;
            label1.Text = "Đơn vị tính";
            // 
            // cbbNhomHangHoa
            // 
            cbbNhomHangHoa.BackColor = Color.Transparent;
            cbbNhomHangHoa.CustomizableEdges = customizableEdges21;
            cbbNhomHangHoa.DrawMode = DrawMode.OwnerDrawFixed;
            cbbNhomHangHoa.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbNhomHangHoa.FocusedColor = Color.FromArgb(94, 148, 255);
            cbbNhomHangHoa.FocusedState.BorderColor = Color.FromArgb(94, 148, 255);
            cbbNhomHangHoa.Font = new Font("Segoe UI", 10F, FontStyle.Regular, GraphicsUnit.Point);
            cbbNhomHangHoa.ForeColor = Color.FromArgb(68, 88, 112);
            cbbNhomHangHoa.ItemHeight = 37;
            cbbNhomHangHoa.Location = new Point(138, 140);
            cbbNhomHangHoa.Name = "cbbNhomHangHoa";
            cbbNhomHangHoa.ShadowDecoration.CustomizableEdges = customizableEdges22;
            cbbNhomHangHoa.Size = new Size(325, 43);
            cbbNhomHangHoa.TabIndex = 0;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(11, 150);
            label7.Name = "label7";
            label7.Size = new Size(116, 20);
            label7.TabIndex = 49;
            label7.Text = "Nhóm hàng hóa";
            // 
            // checkboxHangHoa
            // 
            checkboxHangHoa.AutoSize = true;
            checkboxHangHoa.CheckedState.BorderColor = Color.FromArgb(94, 148, 255);
            checkboxHangHoa.CheckedState.BorderRadius = 0;
            checkboxHangHoa.CheckedState.BorderThickness = 0;
            checkboxHangHoa.CheckedState.FillColor = Color.FromArgb(94, 148, 255);
            checkboxHangHoa.Location = new Point(877, 376);
            checkboxHangHoa.Name = "checkboxHangHoa";
            checkboxHangHoa.Size = new Size(90, 24);
            checkboxHangHoa.TabIndex = 7;
            checkboxHangHoa.Text = "Disabled";
            checkboxHangHoa.UncheckedState.BorderColor = Color.FromArgb(125, 137, 149);
            checkboxHangHoa.UncheckedState.BorderRadius = 0;
            checkboxHangHoa.UncheckedState.BorderThickness = 0;
            checkboxHangHoa.UncheckedState.FillColor = Color.White;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(515, 376);
            label8.Name = "label8";
            label8.Size = new Size(35, 20);
            label8.TabIndex = 52;
            label8.Text = "Ảnh";
            // 
            // picHangHoa
            // 
            picHangHoa.CustomizableEdges = customizableEdges23;
            picHangHoa.ImageRotate = 0F;
            picHangHoa.InitialImage = null;
            picHangHoa.Location = new Point(642, 366);
            picHangHoa.Name = "picHangHoa";
            picHangHoa.ShadowDecoration.CustomizableEdges = customizableEdges24;
            picHangHoa.Size = new Size(148, 148);
            picHangHoa.SizeMode = PictureBoxSizeMode.Zoom;
            picHangHoa.TabIndex = 53;
            picHangHoa.TabStop = false;
            picHangHoa.Click += picHangHoa_Click;
            // 
            // FrmChiTietHangHoa1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1015, 611);
            Controls.Add(picHangHoa);
            Controls.Add(label8);
            Controls.Add(checkboxHangHoa);
            Controls.Add(cbbNhomHangHoa);
            Controls.Add(label7);
            Controls.Add(cbbNhaCungCap);
            Controls.Add(label6);
            Controls.Add(guna2Panel1);
            Controls.Add(guna2Panel2);
            Controls.Add(txtDonGia);
            Controls.Add(label5);
            Controls.Add(txtDonViTinh);
            Controls.Add(txtSoLuong);
            Controls.Add(label4);
            Controls.Add(txtTenHangHoa);
            Controls.Add(label3);
            Controls.Add(txtBarcode);
            Controls.Add(label2);
            Controls.Add(label1);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmChiTietHangHoa1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FrmChiTietHangHoa1";
            guna2Panel1.ResumeLayout(false);
            guna2Panel1.PerformLayout();
            guna2Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)picHangHoa).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Guna.UI2.WinForms.Guna2Button btnDongY;
        private Guna.UI2.WinForms.Guna2ComboBox cbbNhaCungCap;
        private Label label6;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Label lblTieuDe;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Button btnBoQua;
        private Guna.UI2.WinForms.Guna2TextBox txtDonGia;
        private Label label5;
        private Guna.UI2.WinForms.Guna2TextBox txtDonViTinh;
        private Guna.UI2.WinForms.Guna2TextBox txtSoLuong;
        private Label label4;
        private Guna.UI2.WinForms.Guna2TextBox txtTenHangHoa;
        private Label label3;
        private Guna.UI2.WinForms.Guna2TextBox txtBarcode;
        private Label label2;
        private Label label1;
        private Guna.UI2.WinForms.Guna2ComboBox cbbNhomHangHoa;
        private Label label7;
        private Guna.UI2.WinForms.Guna2CheckBox checkboxHangHoa;
        private Label label8;
        private Guna.UI2.WinForms.Guna2PictureBox picHangHoa;
    }
}