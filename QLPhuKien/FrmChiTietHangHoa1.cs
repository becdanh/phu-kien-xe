﻿using Microsoft.VisualBasic.Logging;
using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmChiTietHangHoa1 : Form
    {
        HangHoaDTO hanghoa;
        public FrmChiTietHangHoa1(HangHoaDTO hanghoa = null)
        {
            InitializeComponent();
            this.hanghoa = hanghoa;
            LoadNhomHangHoa();
            LoadNhaCungCap();
            if (hanghoa != null)
            {
                lblTieuDe.Text = "CẬP NHẬT HÀNG HÓA";
                cbbNhomHangHoa.SelectedValue = hanghoa.MaNHH;
                txtBarcode.Text = hanghoa.Barcode;
                txtTenHangHoa.Text = hanghoa.TenHangHoa;
                txtDonGia.Text = hanghoa.GiaBan.ToString();
                txtDonViTinh.Text = hanghoa.DonVi;
                cbbNhaCungCap.SelectedValue = hanghoa.MaNCC;
                picHangHoa.ImageLocation = hanghoa.Anh;
                txtSoLuong.Text = hanghoa.SoLuong.ToString();
                txtBarcode.Enabled = false;
                checkboxHangHoa.Checked = hanghoa.Disabled;
            }
        }

        void LoadNhomHangHoa()
        {
            using (var db = new PhuKienDB())
            {
                var ls = db.nhomHangHoas.Select(e => new NhomHangHoaDTO
                {
                    MaNHH = e.MaNHH,
                    TenNHH = e.TenNHH,
                }).ToList();
                cbbNhomHangHoa.DataSource = ls;
                cbbNhomHangHoa.DisplayMember = "TenNHH";
                cbbNhomHangHoa.ValueMember = "MaNHH";
            }
        }

        void LoadNhaCungCap()
        {
            using (var db = new PhuKienDB())
            {
                var ls = db.nhaCungCaps.Select(e => new NhaCungCapDTO
                {
                    MaNCC = e.MaNCC,
                    TenNCC = e.TenNCC,
                }).ToList();
                cbbNhaCungCap.DataSource = ls;
                cbbNhaCungCap.DisplayMember = "TenNCC";
                cbbNhaCungCap.ValueMember = "MaNCC";
            }
        }
        private bool IsNumeric(string input)
        {
            // Check if the input is a numeric value
            return decimal.TryParse(input, out _);
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            using (var db = new PhuKienDB())
            {
                var nhom = cbbNhomHangHoa.SelectedItem as NhomHangHoaDTO;
                var nhacc = cbbNhaCungCap.SelectedItem as NhaCungCapDTO;
                if (hanghoa == null)
                {
                    if (string.IsNullOrWhiteSpace(txtBarcode.Text)
                        || string.IsNullOrWhiteSpace(txtTenHangHoa.Text)
                        || string.IsNullOrWhiteSpace(txtDonGia.Text)
                        || string.IsNullOrWhiteSpace(txtDonViTinh.Text)
                        || string.IsNullOrWhiteSpace(txtSoLuong.Text)
                        || cbbNhomHangHoa.SelectedItem == null
                        || cbbNhaCungCap.SelectedItem == null
                        || string.IsNullOrWhiteSpace(picHangHoa.ImageLocation))
                    {
                        MessageBox.Show("Vui lòng nhập đầy đủ thông tin và chọn ảnh.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (!IsNumeric(txtDonGia.Text))
                    {
                        MessageBox.Show("Giá bán không hợp lệ. Vui lòng nhập một giá trị số.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsNumeric(txtSoLuong.Text))
                    {
                        MessageBox.Show("Số lượng không hợp lệ. Vui lòng nhập một giá trị số.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var sv = new HangHoa
                    {
                        Barcode = txtBarcode.Text,
                        TenHangHoa = txtTenHangHoa.Text,
                        GiaBan = decimal.Parse(txtDonGia.Text),
                        DonVi = txtDonViTinh.Text,
                        SoLuong = int.Parse(txtSoLuong.Text),
                        MaNHH = nhom.MaNHH,
                        MaNCC = nhacc.MaNCC,
                        Disabled = checkboxHangHoa.Checked,
                        Anh = picHangHoa.ImageLocation
                    };
                    db.hangHoas.Add(sv);
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;

                }
                else
                {
                    if (!IsNumeric(txtDonGia.Text))
                    {
                        MessageBox.Show("Giá bán không hợp lệ. Vui lòng nhập một giá trị số.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsNumeric(txtSoLuong.Text))
                    {
                        MessageBox.Show("Số lượng không hợp lệ. Vui lòng nhập một giá trị số.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var sv = db.hangHoas.Where(t => t.Barcode == hanghoa.Barcode).FirstOrDefault();
                    sv.TenHangHoa = txtTenHangHoa.Text;
                    sv.GiaBan = decimal.Parse(txtDonGia.Text);
                    sv.DonVi = txtDonViTinh.Text;
                    sv.SoLuong = int.Parse(txtSoLuong.Text);
                    sv.MaNHH = nhom.MaNHH;
                    sv.MaNCC = nhacc.MaNCC;
                    sv.Disabled = checkboxHangHoa.Checked;
                    sv.Anh = picHangHoa.ImageLocation;
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void picHangHoa_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.jpg; *.jpeg; *.png; *.gif; *.bmp)|*.jpg; *.jpeg; *.png; *.gif; *.bmp";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                picHangHoa.ImageLocation = openFileDialog.FileName;
            }
        }
    }
}
