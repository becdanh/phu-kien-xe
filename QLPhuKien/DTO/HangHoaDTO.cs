﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.DTO
{
    public class HangHoaDTO
    {

        public string Barcode { get; set; }
        public string TenHangHoa { get; set; } 
        public string DonVi { get; set; }
        public decimal GiaBan { get; set; }
        public int SoLuong { get; set; }
        public string MaNHH { get; set; }
        public string MaNCC { get; set; }
        public string Anh { get; set; }
        public bool Disabled { get; set; }
    }
}
