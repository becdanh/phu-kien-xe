﻿using QLPhuKien.Models;

namespace QLPhuKien.DTO
{
    public class HoaDonDTO
    {
        public int HoaDonId { get; set; }
        public DateTime NgayTao { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public decimal TongTien { get; set; }
        public string TenNhanVien { get; set; }
        public string TenKhachHang { get; set; }
    }
}
