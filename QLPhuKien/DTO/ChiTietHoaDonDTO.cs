﻿using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.DTO
{
    public class ChiTietHoaDonDTO
    {
        public int ChiTietHoaDonId { get; set; }
        public int HoaDonId { get; set; }
        public string Barcode { get; set; }
        public string TenHH { get; set; }
        public int SoLuongMua { get; set; }
        public decimal GiaBan { get; set; }
        public decimal ThanhTien { 
            get { return GiaBan * SoLuongMua; }
        }
    }
}
