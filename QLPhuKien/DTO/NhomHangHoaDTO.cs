﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.DTO
{
    public class NhomHangHoaDTO
    {
        public string MaNHH { get; set; }
        public string TenNHH { get; set; }
        public string GhiChu { get; set; }
    }
}
