﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLPhuKien.DTO
{
    public class KhachHangDTO
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string SDT { get; set; }
        public string DiaChi { get; set; }
    }
}
