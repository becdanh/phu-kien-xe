﻿namespace QLPhuKien
{
    partial class FrmDashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            labelClock = new Label();
            timer1 = new System.Windows.Forms.Timer(components);
            labelSecond = new Label();
            SuspendLayout();
            // 
            // labelClock
            // 
            labelClock.Anchor = AnchorStyles.None;
            labelClock.AutoSize = true;
            labelClock.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            labelClock.Location = new Point(396, 237);
            labelClock.Name = "labelClock";
            labelClock.Size = new Size(180, 41);
            labelClock.TabIndex = 0;
            labelClock.Text = "02/06/2002";
            // 
            // timer1
            // 
            timer1.Tick += timer1_Tick;
            // 
            // labelSecond
            // 
            labelSecond.Anchor = AnchorStyles.None;
            labelSecond.AutoSize = true;
            labelSecond.Font = new Font("Segoe UI", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            labelSecond.Location = new Point(448, 287);
            labelSecond.Name = "labelSecond";
            labelSecond.Size = new Size(72, 23);
            labelSecond.TabIndex = 1;
            labelSecond.Text = "11:09:40";
            // 
            // FrmDashBoard
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(940, 662);
            Controls.Add(labelSecond);
            Controls.Add(labelClock);
            FormBorderStyle = FormBorderStyle.None;
            Name = "FrmDashBoard";
            Text = "FrmDashBoard";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label labelClock;
        private System.Windows.Forms.Timer timer1;
        private Label labelSecond;
    }
}