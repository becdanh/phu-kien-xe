﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmNhanVien : Form
    {
        public FrmNhanVien()
        {
            InitializeComponent();
            lblTenNhanVien.Text = $"Xin chào: {FrmDangNhap.LoggedInUser.FullName}";
        }
        void LoadNhanVien()
        {

            PhuKienDB db = new PhuKienDB();
            var ls = db.users
                .Select(t => new UserDTO
                {
                    UserId = t.UserId,
                    FullName = t.FullName,
                    Username = t.Username,
                    Password = t.Password,
                    IsAdmin = t.IsAdmin,
                }).ToList();
            userDTOBindingSource.DataSource = ls;

        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            var f = new FrmChiTietNhanVien();
            if (f.ShowDialog() == DialogResult.OK)
            {
                LoadNhanVien();
            }
        }

        private void FrmNhanVien_Load(object sender, EventArgs e)
        {
            LoadNhanVien();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            var currentUser = FrmDangNhap.LoggedInUser;
            var selectedUser = userDTOBindingSource.Current as UserDTO;

            if (currentUser != null && selectedUser != null)
            {
                // Check if the user being edited is not an admin or is the same user who is currently logged in
                if (!selectedUser.IsAdmin || currentUser.UserId == selectedUser.UserId)
                {
                    var f = new FrmChiTietNhanVien(selectedUser);
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        LoadNhanVien();
                    }
                }
                else
                {
                    MessageBox.Show("Không thể chỉnh sửa thông tin của người quản trị khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }



        private void txtTimKiem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                var keyword = txtTimKiem.Text.ToLower();

                using (var db = new PhuKienDB())
                {
                    var filteredList = db.users
                        .Where(u =>
                            u.FullName.ToLower().Contains(keyword) ||
                            u.Username.ToLower().Contains(keyword) ||
                            u.IsAdmin.ToString().ToLower().Contains(keyword) // Include other properties as needed
                        )
                        .Select(u => new UserDTO
                        {
                            UserId = u.UserId,
                            FullName = u.FullName,
                            Username = u.Username,
                            Password = u.Password,
                            IsAdmin = u.IsAdmin,
                        })
                        .ToList();

                    userDTOBindingSource.DataSource = null;
                    userDTOBindingSource.DataSource = filteredList;
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var currentUser = FrmDangNhap.LoggedInUser;
            var selectedUser = userDTOBindingSource.Current as UserDTO;

            if (currentUser != null && selectedUser != null)
            {
                if (currentUser.UserId == selectedUser.UserId)
                {
                    MessageBox.Show("Không thể xóa tài khoản đang đăng nhập.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (selectedUser.IsAdmin)
                    {
                        MessageBox.Show("Không thể xóa thông tin của người quản trị khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn xóa người dùng này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            using (var db = new PhuKienDB())
                            {
                                var userToDelete = db.users.FirstOrDefault(u => u.UserId == selectedUser.UserId);

                                if (userToDelete != null)
                                {
                                    db.users.Remove(userToDelete);
                                    db.SaveChanges();
                                    MessageBox.Show("Xóa người dùng thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    LoadNhanVien(); // Reload the user list after deletion
                                }
                                else
                                {
                                    MessageBox.Show("Không tìm thấy người dùng để xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
