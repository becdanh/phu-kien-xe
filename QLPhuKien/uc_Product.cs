﻿using QLPhuKien.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class uc_Product : UserControl
    {
        public event EventHandler<ProductClickedEventArgs> ProductClicked;
        public HangHoaDTO DisplayedProduct { get; private set; }
        public uc_Product()
        {
            InitializeComponent();
            picProduct.Click += picProduct_Click;
        }

        public void DisplayProductInformation(HangHoaDTO product)
        {
            DisplayedProduct = product; // Set thông tin sản phẩm
            lblProduct.Text = product.TenHangHoa;
            lblTien.Text = product.GiaBan.ToString("N0") + " VNĐ";
            picProduct.Image = Image.FromFile(product.Anh);
        }

        private void picProduct_Click(object sender, EventArgs e)
        {
            OnProductClicked();
        }

        protected virtual void OnProductClicked()
        {
            ProductClicked?.Invoke(this, new ProductClickedEventArgs
            {
                ProductInfo = DisplayedProduct
            });
        }
    }

    public class ProductClickedEventArgs : EventArgs
    {
        public HangHoaDTO ProductInfo { get; set; }
    }
}
