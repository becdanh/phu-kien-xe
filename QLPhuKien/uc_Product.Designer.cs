﻿namespace QLPhuKien
{
    partial class uc_Product
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges1 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            Guna.UI2.WinForms.Suite.CustomizableEdges customizableEdges2 = new Guna.UI2.WinForms.Suite.CustomizableEdges();
            guna2ShadowPanel1 = new Guna.UI2.WinForms.Guna2ShadowPanel();
            panel1 = new Panel();
            lblTien = new Label();
            lblProduct = new Label();
            picProduct = new Guna.UI2.WinForms.Guna2PictureBox();
            guna2ShadowPanel1.SuspendLayout();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)picProduct).BeginInit();
            SuspendLayout();
            // 
            // guna2ShadowPanel1
            // 
            guna2ShadowPanel1.BackColor = Color.Transparent;
            guna2ShadowPanel1.Controls.Add(panel1);
            guna2ShadowPanel1.Controls.Add(picProduct);
            guna2ShadowPanel1.FillColor = Color.White;
            guna2ShadowPanel1.Location = new Point(3, 3);
            guna2ShadowPanel1.Name = "guna2ShadowPanel1";
            guna2ShadowPanel1.ShadowColor = Color.Black;
            guna2ShadowPanel1.Size = new Size(206, 243);
            guna2ShadowPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            panel1.Controls.Add(lblTien);
            panel1.Controls.Add(lblProduct);
            panel1.Dock = DockStyle.Bottom;
            panel1.Location = new Point(0, 168);
            panel1.Name = "panel1";
            panel1.Size = new Size(206, 75);
            panel1.TabIndex = 1;
            // 
            // lblTien
            // 
            lblTien.Dock = DockStyle.Bottom;
            lblTien.Location = new Point(0, 37);
            lblTien.Name = "lblTien";
            lblTien.Size = new Size(206, 38);
            lblTien.TabIndex = 1;
            lblTien.Text = "Số tiền";
            lblTien.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblProduct
            // 
            lblProduct.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lblProduct.Location = new Point(0, 0);
            lblProduct.Name = "lblProduct";
            lblProduct.Size = new Size(206, 38);
            lblProduct.TabIndex = 0;
            lblProduct.Text = "Tên hàng hóa";
            lblProduct.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // picProduct
            // 
            picProduct.CustomizableEdges = customizableEdges1;
            picProduct.Image = Properties.Resources.vk2;
            picProduct.ImageRotate = 0F;
            picProduct.Location = new Point(3, 29);
            picProduct.Name = "picProduct";
            picProduct.ShadowDecoration.CustomizableEdges = customizableEdges2;
            picProduct.Size = new Size(195, 133);
            picProduct.SizeMode = PictureBoxSizeMode.Zoom;
            picProduct.TabIndex = 0;
            picProduct.TabStop = false;
            // 
            // uc_Product
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(guna2ShadowPanel1);
            Name = "uc_Product";
            Size = new Size(209, 249);
            guna2ShadowPanel1.ResumeLayout(false);
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)picProduct).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Guna.UI2.WinForms.Guna2ShadowPanel guna2ShadowPanel1;
        private Guna.UI2.WinForms.Guna2PictureBox picProduct;
        private Panel panel1;
        private Label lblTien;
        private Label lblProduct;
    }
}
