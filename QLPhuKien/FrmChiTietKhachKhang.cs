﻿using QLPhuKien.DTO;
using QLPhuKien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPhuKien
{
    public partial class FrmChiTietKhachKhang : Form
    {
        KhachHangDTO khachhang;
        public FrmChiTietKhachKhang(KhachHangDTO khachhang = null)
        {
            InitializeComponent();
            this.khachhang = khachhang;
            if (khachhang != null)
            {
                //Cap nhat
                lblTieuDe.Text = "CẬP NHẬT KHÁCH HÀNG";
                txtTenKhachHang.Text = khachhang.FullName;
                txtSDT.Text = khachhang.SDT;
                txtDiaChi.Text = khachhang.DiaChi;
            }
        }
        private bool IsPhoneNumberValid(string phoneNumber)
        {
            string phonePattern = @"^\d{10}$"; // Assumes a 10-digit phone number
            return Regex.IsMatch(phoneNumber, phonePattern);
        }
        private void btnDongY_Click(object sender, EventArgs e)
        {
            using (var db = new PhuKienDB())
            {
                if (khachhang == null)
                {
                    if (string.IsNullOrWhiteSpace(txtTenKhachHang.Text)
                    || string.IsNullOrWhiteSpace(txtDiaChi.Text)
                    || string.IsNullOrWhiteSpace(txtSDT.Text))
                    {
                        MessageBox.Show("Vui lòng nhập đầy đủ thông tin.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Check for duplicate phone number
                    if (db.khachHangs.Any(t => t.SDT == txtSDT.Text))
                    {
                        MessageBox.Show("Số điện thoại đã tồn tại. Vui lòng chọn số điện thoại khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsPhoneNumberValid(txtSDT.Text))
                    {
                        MessageBox.Show("Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var kh = new KhachHang
                    {
                        FullName = txtTenKhachHang.Text,
                        SDT = txtSDT.Text,
                        DiaChi = txtDiaChi.Text,
                    };
                    db.khachHangs.Add(kh);
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;

                }
                else
                {
                    // Check for duplicate phone number (excluding the current record being edited)
                    if (db.khachHangs.Any(t => t.SDT == txtSDT.Text && t.CustomerId != khachhang.CustomerId))
                    {
                        MessageBox.Show("Số điện thoại đã tồn tại. Vui lòng chọn số điện thoại khác.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (!IsPhoneNumberValid(txtSDT.Text))
                    {
                        MessageBox.Show("Số điện thoại không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    var kh = db.khachHangs.Where(t => t.CustomerId == khachhang.CustomerId).FirstOrDefault();
                    kh.FullName = txtTenKhachHang.Text;
                    kh.SDT = txtSDT.Text;
                    kh.DiaChi = txtDiaChi.Text;
                    db.SaveChanges();
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
